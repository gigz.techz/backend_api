package com.studio.photo.post;

import lombok.*;

import java.util.List;

@Getter
@Setter
public class CommentDTO {
    private Integer commentId;
    private String username;
    private String profilePic;
    private String commentText;
    private boolean hasReplies;
    private List<CommentDTO> replies;


}
