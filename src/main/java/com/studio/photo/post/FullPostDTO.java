package com.studio.photo.post;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FullPostDTO {
    private Integer postId;
    private String postCreatedBy;
    private String profilePic;
    private String content;
    private int commentCount;
    private int likeCount;
    private List<String> tags;
    private List<MentionDTO> mentions;
    private List<MediaDTO> mediaList;
    private boolean likedOrNot;
    private boolean commentOrNot;
}

