package com.studio.photo.post;

import com.studio.photo.dto.PostDTOResponse;
import com.studio.photo.entity.*;
import com.studio.photo.file.FileHandlingService;
import com.studio.photo.repeat.Response;
import com.studio.photo.repository.*;
import com.studio.photo.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RequiredArgsConstructor
@Service
public class PostService {


    private final FileHandlingService fileHandlingService;
    private final UserDTO userDTO;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final LikeRepository likeRepository;


    @Transactional
    public Response createPost(PostRequest postRequest) {
        try {
            User currentUser = userDTO.getCurrentLoginUser();
            Post post = Post.builder()
                    .userId(currentUser)
                    .content(postRequest.getContent())
                    .tags(postRequest.getTags())
                    .mediaList(new ArrayList<>())
                    .build();

            List<Media> mediaList = new ArrayList<>();

            for (String path : postRequest.getPath()) {
                Media media = new Media();
                media.setPath(path);
                media.setPost(post);
                mediaList.add(media);
            }

            post.getMediaList().addAll(mediaList);

            List<Mention> mentions = new ArrayList<>();

            for (Integer mentionId : postRequest.getMentions()) {
                Optional<User> mentionedUserOptional = userRepository.findById(mentionId);
                if (mentionedUserOptional.isPresent()) {
                    User mentionedUser = mentionedUserOptional.get();
                    Mention mention = new Mention(mentionedUser, post);
                    mentions.add(mention);
                } else {
                    throw new RuntimeException("User with id " + mentionId + " not found");
                }
            }

            post.setMentions(mentions);
//            long currentCount= currentUser.getBiography().getPostCount();
//            long newCount= currentCount+1;
//            currentUser.getBiography().setPostCount(newCount);
            // Checked if PostCount exists in biography table
            Biography biography = currentUser.getBiography();
            if (biography != null) {
                long currentCount = biography.getPostCount();
                biography.setPostCount(currentCount + 1);
            }
            postRepository.save(post);

            return new Response("Post created successfully");
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Transactional
    public List<FullPostDTO> getPostFeed(Pageable page) {
        long totalCount = postRepository.count();
        List<Integer> randomIndices = getRandomIndices(totalCount, page.getPageSize());
        Page<Post> postPage = postRepository.findByIdIn(randomIndices, page);

        return postPage.getContent().stream()
                .map(this::mapToFullPostDTOWithoutCommentsAndLikes)
                .collect(Collectors.toList());
    }

    private FullPostDTO mapToFullPostDTOWithoutCommentsAndLikes(Post post) {
        FullPostDTO fullPostDTO = new FullPostDTO();
        fullPostDTO.setPostId(post.getId());
        PostDTOResponse postDTOResponse = userDTO.getUserIdAndNameByUserId(post.getUserId());
        fullPostDTO.setProfilePic(postDTOResponse.getProfilePicPath());
        fullPostDTO.setPostCreatedBy(postDTOResponse.getName());
        fullPostDTO.setContent(post.getContent());
        fullPostDTO.setTags(post.getTags());
        fullPostDTO.setCommentCount(post.getComments().size());
        fullPostDTO.setLikeCount(post.getLikes().size());
        boolean userCommentOrNot= getUserCommentOrNotInThisPostId(post.getId(),userDTO.getCurrentLoginUser().getId());
        boolean userLikeOrNot= getUserLikeOrNotInThisPostId(post.getId(),userDTO.getCurrentLoginUser().getId());
        fullPostDTO.setCommentOrNot(userCommentOrNot);
        fullPostDTO.setLikedOrNot(userLikeOrNot);

        List<MentionDTO> mentions = post.getMentions().stream()
                .map(mention -> new MentionDTO(mention.getMentionedUser().getUsername(), userDTO.returnNameByUsername(mention.getMentionedUser().getUsername())))
                .collect(Collectors.toList());

        fullPostDTO.setMentions(mentions);

        List<MediaDTO> mediaList = post.getMediaList().stream()
                .map(media -> new MediaDTO(media.getId(), media.getPath()))
                .collect(Collectors.toList());
        fullPostDTO.setMediaList(mediaList);

        return fullPostDTO;
    }

    private boolean getUserLikeOrNotInThisPostId(Integer postId, Integer userId) {
            return likeRepository.existsByUser_IdAndPost_Id(userId,postId);
    }

    private boolean getUserCommentOrNotInThisPostId(Integer postId, Integer userId) {
            return commentRepository.existsByUser_IdAndPost_Id(userId,postId);
    }

    private List<Integer> getRandomIndices(long totalCount, int pageSize) {
        List<Integer> allIndices = IntStream.range(0, (int) totalCount)
                .boxed()
                .collect(Collectors.toList());

        Collections.shuffle(allIndices);
        return allIndices.subList(0, Math.min(pageSize, (int) totalCount));
    }

    public Response createPostComment(Integer postId, CommentRequest commentRequest) {
        try
        {
            var post= postRepository.findById(postId).orElseThrow(
                    () -> new RuntimeException("Post not found with ID: " + postId)
            );
            User currentUser = userDTO.getCurrentLoginUser();
            var comment= Comment.builder()
                    .post(post)
                    .text(commentRequest.getCommentContent())
                    .user(currentUser)
                    .build();
            var postComment=commentRepository.save(comment);

            post.getComments().add(postComment);
            postRepository.save(post);

            return Response.builder()
                    .message("Comment created successfully")
                    .build();

        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    @Transactional
    public Response createReplyComment(Integer postId, Integer parentCommentId, CommentRequest commentRequest) {
        try {
            var post = postRepository.findById(postId).orElseThrow(
                    () -> new RuntimeException("Post not found with ID: " + postId)
            );
            var parentComment = commentRepository.findById(parentCommentId).orElseThrow(
                    () -> new RuntimeException("Parent comment not found with ID: " + parentCommentId)
            );
            User currentUser = userDTO.getCurrentLoginUser();

            var replyComment = Comment.builder()
                    .post(post)
                    .text(commentRequest.getCommentContent())
                    .user(currentUser)
                    .parentComment(parentComment)
                    .build();

            var savedReplyComment = commentRepository.save(replyComment);

            parentComment.getReplies().add(savedReplyComment);
            commentRepository.save(parentComment);

            return Response.builder()
                    .message("Reply comment created successfully")
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    @Transactional
    public Response editComment(Integer commentId, CommentRequest commentRequest) {
        try {
            Comment comment = commentRepository.findById(commentId)
                    .orElseThrow(() -> new RuntimeException("Comment not found with ID: " + commentId));

            User currentUser = userDTO.getCurrentLoginUser();
            if (!currentUser.equals(comment.getUser())) {
                throw new RuntimeException("You are not authorized to edit this comment.");
            }

            comment.setText(commentRequest.getCommentContent());
            commentRepository.save(comment);
            return new Response("Comment edited successfully");
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    @Transactional
    public Response deleteComment(Integer commentId) {
        try {
            Comment comment = commentRepository.findById(commentId)
                    .orElseThrow(() -> new RuntimeException("Comment not found with ID: " + commentId));

            User currentUser = userDTO.getCurrentLoginUser();
            if (!currentUser.equals(comment.getUser()) && !currentUser.equals(comment.getPost().getUserId())) {
                throw new RuntimeException("You are not authorized to delete this comment.");
            }

            commentRepository.delete(comment);
            return new Response("Comment deleted successfully");
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    @Transactional
    public Response likeDislikePost(Integer postId) {
        try {
            User currentUser = userDTO.getCurrentLoginUser();
            Post post = postRepository.findById(postId)
                    .orElseThrow(() -> new RuntimeException("Post not found with ID: " + postId));

            Like existingLike = post.getLikes().stream()
                    .filter(like -> like.getUser().equals(currentUser))
                    .findFirst()
                    .orElse(null);

            if (existingLike != null) {
                post.getLikes().remove(existingLike);
                likeRepository.delete(existingLike);
                postRepository.save(post);
                return new Response("Post disliked successfully");
            } else {
                Like like = new Like();
                like.setUser(currentUser);
                like.setPost(post);
                likeRepository.save(like);
                post.getLikes().add(like);
                postRepository.save(post);
                return new Response("Post liked successfully");
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Transactional
    public Response deletePost(Integer postId) {
        try {
            User currentUser = userDTO.getCurrentLoginUser();
            Post post = postRepository.findById(postId)
                    .orElseThrow(() -> new RuntimeException("Post not found with ID: " + postId));
            if (!post.getUserId().equals(currentUser)) {
                throw new RuntimeException("You do not have permission to delete this post");
            }
            postRepository.deleteById(postId);
            return new Response("Post deleted successfully");
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    public CommentResponse fetchComments(Integer postId) {
        try {
            Post post = postRepository.findById(postId)
                    .orElseThrow(() -> new RuntimeException("Post not found with ID: " + postId));

                List<Comment> comments = commentRepository.findByPostAndParentCommentIsNull(post);
                List<CommentDTO> commentDTOs = comments.stream()
                        .map(this::mapToCommentDTO)
                        .collect(Collectors.toList());

                CommentResponse response = new CommentResponse();
                response.setData("success");
                response.setComments(commentDTOs);
                return response;

        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    private CommentDTO mapToCommentDTO(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setCommentId(comment.getId());
        PostDTOResponse postDTOResponse = userDTO.getUserIdAndNameByUserId(comment.getUser());
        commentDTO.setUsername(comment.getUser().getUsername());
        commentDTO.setProfilePic(postDTOResponse.getProfilePicPath());
        commentDTO.setCommentText(comment.getText());
        boolean hasReplies = comment.getReplies() != null && !comment.getReplies().isEmpty();
        commentDTO.setHasReplies(hasReplies);
        return commentDTO;
    }

    public CommentResponse fetchReply(Integer commentId) {
        try {
            Comment comment = commentRepository.findById(commentId)
                    .orElseThrow(ChangeSetPersister.NotFoundException::new);

            List<CommentDTO> comments = mapCommentToDTOWithReplies(comment);
            CommentResponse response = new CommentResponse();
            response.setData("success");
            response.setComments(comments);
            return response;
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    private List<CommentDTO> mapCommentToDTOWithReplies(Comment comment) {
        return comment.getReplies().stream()
                .map(this::mapCommentToDTOWithRepliesRecursive)
                .collect(Collectors.toList());
    }

    private CommentDTO mapCommentToDTOWithRepliesRecursive(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setCommentId(comment.getId());
        commentDTO.setCommentText(comment.getText());
        commentDTO.setHasReplies(!comment.getReplies().isEmpty());

        commentDTO.setReplies(mapCommentToDTOWithReplies(comment));

        return commentDTO;
    }
}
