package com.studio.photo.post;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
public class PostRequest {
    private List<String> path;
    private String content;
    private List<String> tags;
    private List<Integer> mentions;
}
