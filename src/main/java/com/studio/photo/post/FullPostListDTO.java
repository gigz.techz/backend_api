package com.studio.photo.post;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FullPostListDTO {
    private String data;
    private List<FullPostDTO> posts;
}

