package com.studio.photo.post;

import com.studio.photo.repeat.Response;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/protected/post")
@RequiredArgsConstructor
public class PostController {
    private final PostService postService;
    @PostMapping("create")
    public Response createPost(
            @RequestBody @Valid PostRequest postRequest,
            @NonNull HttpServletRequest httpServletRequest) {
        return postService.createPost(postRequest);
    }

    @PostMapping("create/comment")
    public Response createPostComment(
            @RequestParam("postId") Integer postId,
            @RequestBody@Valid CommentRequest commentRequest) {
        return postService.createPostComment(postId,commentRequest);
    }

    @GetMapping("comment-fetch")
    public CommentResponse fetchComment(
            @RequestParam("postId") Integer postId) {
        return postService.fetchComments(postId);
    }

    @GetMapping("reply-fetch")
    public CommentResponse fetchReply(
            @RequestParam("commentId") Integer commentId) {
        return postService.fetchReply(commentId);
    }

    @PostMapping("/{postId}/comments/{parentCommentId}/reply")
    public ResponseEntity<Response> createReplyComment(
            @PathVariable Integer postId,
            @PathVariable Integer parentCommentId,
            @RequestBody CommentRequest commentRequest) {
        Response response = postService.createReplyComment(postId, parentCommentId, commentRequest);
        return ResponseEntity.ok(response);
    }


    @PutMapping("comments/{commentId}")
    public ResponseEntity<Response> editComment(
            @PathVariable Integer commentId,
            @RequestBody CommentRequest commentRequest) {
        Response response = postService.editComment(commentId, commentRequest);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("comments/{commentId}")
    public ResponseEntity<Response> deleteComment(
            @PathVariable Integer commentId) {
        Response response = postService.deleteComment(commentId);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("delete/{postId}")
    public ResponseEntity<Response> deletePost(
            @PathVariable Integer postId) {
        Response response = postService.deletePost(postId);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/{postId}/like-dislike")
    public ResponseEntity<Response> likeDislikePost(@PathVariable Integer postId) {
        Response response = postService.likeDislikePost(postId);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/feed")
    public ResponseEntity<FullPostListDTO> getPostFeed(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        List<FullPostDTO> posts = postService.getPostFeed(PageRequest.of(page, size));
        FullPostListDTO response = FullPostListDTO.builder()
                .data("success")
                .posts(posts)
                .build();
        return ResponseEntity.ok(response);
    }

}
