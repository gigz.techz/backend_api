package com.studio.photo.post;

import lombok.*;

import java.util.List;

@Getter
@Setter
public class CommentResponse {
    private String data;
    private List<CommentDTO> comments;
}
