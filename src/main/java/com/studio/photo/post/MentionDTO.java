package com.studio.photo.post;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MentionDTO {
    private String id;
    private String name;
}
