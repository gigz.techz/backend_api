package com.studio.photo.init;


import com.studio.photo.entity.Service;
import com.studio.photo.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.List;

@Component

public class DataInitializer implements CommandLineRunner {

    @Autowired
    private ServiceRepository serviceRepository;


    @Override
    public void run(String... args) throws Exception {
        initializeServices();
    }

    private void initializeServices() {
        if (serviceRepository.count() == 0) {
            var photographyService = Service.builder()
                    .serviceName("Photography")
                    .build();
            var preWeddingService = Service.builder()
                    .serviceName("Pre-Wedding Ceremony")
                    .parentService(photographyService)
                    .build();
            var postWeddingService = Service.builder()
                    .serviceName("Post-Wedding")
                    .parentService(photographyService)
                    .build();
            var birthdayService = Service.builder()
                    .serviceName("Birthday")
                    .parentService(photographyService)
                    .build();
            var anniversaryService = Service.builder()
                    .serviceName("Anniversary")
                    .parentService(photographyService)
                    .build();

            serviceRepository.saveAll(List.of(
                    photographyService,
                    preWeddingService, postWeddingService, birthdayService, anniversaryService
            ));
        }
    }
}
