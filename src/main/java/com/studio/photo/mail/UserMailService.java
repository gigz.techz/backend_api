package com.studio.photo.mail;

import jakarta.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserMailService {
    private final JavaMailSender javaMailSender;

    public boolean sendVerificationEmail(String email, String verificationLink) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

            helper.setFrom("rohitchamp19995@gmail.com");
            helper.setTo(email);
            helper.setSubject("Account Verification");

            String emailContent = "Click the following link to verify your account: " + verificationLink;
            helper.setText(emailContent, true);
            javaMailSender.send(mimeMessage);
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }

    }

    public boolean sendOtp(String email, String otp) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

            helper.setFrom("rohitchamp19995@gmail.com");
            helper.setTo(email);
            helper.setSubject("OTP");

            String emailContent = "Paste this OTP to generate new password" + otp;
            helper.setText(emailContent, true);
            javaMailSender.send(mimeMessage);
            return true;
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }

    }

    public boolean sendPassword(String email, String password) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

            helper.setFrom("mail_address@gmail.com");
            helper.setTo(email);
            helper.setSubject("PASSWORD");

            String emailContent = "This is you generated password" + password;
            helper.setText(emailContent, true);
            javaMailSender.send(mimeMessage);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("mail authentication failed: "+e.getLocalizedMessage());
        }

    }


}
