package com.studio.photo.chat;

import com.studio.photo.repeat.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;


@RestController
@RequestMapping("/api/protected/ws")
@RequiredArgsConstructor
public class ChatController {

    private final ChatService chatService;
    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public ChatRequest sendMessage(
            @Payload ChatRequest chatMessage
    ) {
        return chatMessage;
    }

    @MessageMapping("/chat.sendPrivateMessage")
    @SendTo("/user/private")
    public Response sendPrivateMessage(
            @Payload ChatRequest chatRequest,
            SimpMessageHeaderAccessor headerAccessor
    ){
        chatService.savePrivateMessage(chatRequest, headerAccessor);
        return Response.builder()
                .message("success")
                .build();
    }


    @MessageMapping("/chat.addUser")
    @SendTo("/topic/public")
    public ChatRequest addUser(
            @Payload ChatRequest chatMessage,
            SimpMessageHeaderAccessor headerAccessor
    ) {
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        return chatMessage;
    }

    @MessageMapping("/chat.createGroup")
    @SendTo("/topic/public")
    public Response createGroup(
            @Payload GroupRequest groupRequest,
            SimpMessageHeaderAccessor headerAccessor
    ) {
        return chatService.createGroup(groupRequest);
    }

    @MessageMapping("/chat.addParticipantToGroup")
    @SendTo("/topic/public")
    public Response addParticipantToGroup(
            @Payload GroupRequest groupRequest,
            SimpMessageHeaderAccessor headerAccessor
    ) {
        return chatService.addParticipantToGroup(groupRequest.getGroupId(), groupRequest.getParticipantId());
    }

    @MessageMapping("/chat.sendMessageInGroup")
    @SendTo("/topic/public")
    public Response sendMessageInGroup(
            @Payload GroupMessageRequest groupMessageRequest,
            SimpMessageHeaderAccessor headerAccessor
    ) {
        return chatService.sendMessageInGroup(groupMessageRequest);
    }




}


