package com.studio.photo.chat;

import lombok.Data;

import java.util.List;

@Data
public class GroupRequest {
    private String groupName;
    private String groupDescription;
    private String groupImage;
    private Integer groupId;
    private Integer participantId;
    private List<Integer> participantIds;
}

