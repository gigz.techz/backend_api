package com.studio.photo.chat;

import lombok.Data;

@Data
public class GroupMessageRequest {
    private Integer groupId;
    private String content;
}
