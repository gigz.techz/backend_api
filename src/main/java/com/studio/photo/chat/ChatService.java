package com.studio.photo.chat;

import com.studio.photo.chatpoll.GroupDToResponse;
import com.studio.photo.dto.ChatDTO;
import com.studio.photo.dto.ChatMessageDTO;
import com.studio.photo.dto.UserDTO;
import com.studio.photo.entity.ChatMessage;
import com.studio.photo.entity.GroupChat;
import com.studio.photo.entity.User;
import com.studio.photo.repeat.Response;
import com.studio.photo.repository.ChatMessageRepository;
import com.studio.photo.repository.GroupChatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ChatService {

    private final SimpMessagingTemplate messagingTemplate;
    private final ChatMessageRepository chatMessageRepository;
    private final GroupChatRepository groupChatRepository;
    private final UserDTO userDTO;
    private final ChatDTO chatDTO;
    public void savePrivateMessage(ChatRequest chatRequest, SimpMessageHeaderAccessor headerAccessor) {
        User sender = userDTO.getCurrentLoginUser();
        User receiver = userDTO.getUserById(chatRequest.getReceiverId());

        ChatMessage chatMessage = ChatMessage.builder()
                .sender(sender)
                .receiver(receiver)
                .message(chatRequest.getContent())
                .timestamp(LocalDateTime.now())
                .build();

        chatMessageRepository.save(chatMessage);

        ChatMessageDTO chatMessageDTO = chatDTO.mapToDTO(chatMessage);

        messagingTemplate.convertAndSendToUser(
                String.valueOf(sender.getId()),
                "/private",
                chatMessageDTO
        );

        messagingTemplate.convertAndSendToUser(
                String.valueOf(receiver.getId()),
                "/private",
                chatMessageDTO
        );
    }

    public Response createGroup(GroupRequest groupRequest) {
        User currentUser = userDTO.getCurrentLoginUser();

        List<User> participants = userDTO.getUsersByIds(groupRequest.getParticipantIds());

        GroupChat groupChat = GroupChat.builder()
                .groupName(groupRequest.getGroupName())
                .groupPic(groupRequest.getGroupImage())
                .groupDescription(groupRequest.getGroupDescription())
                .admin(currentUser)
                .participants(participants)
                .build();

        groupChat = groupChatRepository.save(groupChat);
        notifyGroupCreation(currentUser, participants, groupChat);


        return Response.builder()
                .message("group created successfully")
                .build();
    }

    private void notifyGroupCreation(User currentUser, List<User> participants, GroupChat groupChat) {
        for (User participant : participants) {
            messagingTemplate.convertAndSendToUser(
                    String.valueOf(participant.getId()),
                    "/private",
                    "You have been added to the group: " + groupChat.getGroupName() + " by " + currentUser.getUsername()
            );
        }

        messagingTemplate.convertAndSendToUser(
                String.valueOf(currentUser.getId()),
                "/private",
                "Group " + groupChat.getGroupName() + " created successfully"
        );
    }

    public Response addParticipantToGroup(Integer groupId, Integer participantId) {
        User participant = userDTO.getUserById(participantId);

        Optional<GroupChat> optionalGroupChat = groupChatRepository.findById(groupId);
        if (optionalGroupChat.isPresent()) {
            GroupChat groupChat = optionalGroupChat.get();

            groupChat.getParticipants().add(participant);

            groupChat = groupChatRepository.save(groupChat);

            return Response.builder()
                    .message("success")
                    .build();
        } else {
           throw new RuntimeException("group does not exist");
        }
    }

    public Response sendMessageInGroup(GroupMessageRequest groupMessageRequest) {
        User currentUser = userDTO.getCurrentLoginUser();
        GroupChat groupChat = groupChatRepository.findById(groupMessageRequest.getGroupId())
                .orElseThrow(() -> new RuntimeException("Group not found"));

        if (!groupChat.getParticipants().contains(currentUser)) {
            throw new RuntimeException("User is not a member of the group");
        }

        ChatMessage chatMessage = ChatMessage.builder()
                .sender(currentUser)
                .groupChat(groupChat)
                .message(groupMessageRequest.getContent())
                .timestamp(LocalDateTime.now())
                .build();

        chatMessageRepository.save(chatMessage);

        messagingTemplate.convertAndSend("/topic/public", chatMessage);

        return Response.builder()
                .message("Message sent in group successfully")
                .build();
    }



}
