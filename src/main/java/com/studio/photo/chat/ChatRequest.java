package com.studio.photo.chat;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatRequest {

    private MessageType type;
    private String content;
    private Integer receiverId;
    private String sender;


}


