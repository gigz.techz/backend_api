package com.studio.photo.chat;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatMessageDTO {
    private Integer id;
    private String senderUsername;
    private String receiverUsername;
    private String message;
    private LocalDateTime timestamp;
    private Integer groupChatId;
}
