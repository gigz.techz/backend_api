package com.studio.photo.chat;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupChatDTO {
    private Integer id;
    private String adminUsername;
    private String groupName;
    private String groupPic;
    private String groupDescription;
    private List<String> participantUsernames;
}
