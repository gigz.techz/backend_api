package com.studio.photo.file;

import com.studio.photo.security.AppProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@RequiredArgsConstructor
@Service
public class FileHandlingService {

    private final S3Client s3Client;
    private final AppProperties appProperties;


    public String saveFile(MultipartFile file) {
        try {
            File tempFile = File.createTempFile("temp", null);
            file.transferTo(tempFile);

            String fileName = System.currentTimeMillis() + file.getOriginalFilename();
            s3Client.putObject(PutObjectRequest.builder()
                    .bucket(appProperties.getBucketName())
                    .key(fileName)
                    .build(), tempFile.toPath());
            tempFile.delete();
            String s3Url = "https://" + appProperties.getBucketName() + ".s3.amazonaws.com/" + fileName;
            return s3Url;
        } catch (IOException e) {
            throw new RuntimeException("Failed to save profile image: " + e.getMessage());
        }
    }


    public FileResponse postProfileImage(List<MultipartFile> files) {
        List<String> fileUrls = new ArrayList<>();

        for (MultipartFile file : files) {
            String fileUrl = saveFile(file);
            fileUrls.add(fileUrl);
        }
        FileResponse fileResponse = new FileResponse();
        fileResponse.setPath(fileUrls);
        return fileResponse;
    }
}
