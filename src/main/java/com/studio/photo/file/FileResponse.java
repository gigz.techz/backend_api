package com.studio.photo.file;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FileResponse {
    private List<String> path;
}
