package com.studio.photo.file;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.net.UnknownHostException;
import java.util.List;

@RestController
@RequestMapping("/api/public/file/")
@RequiredArgsConstructor
public class FileController {

    private final FileHandlingService fileHandlingService;
    @PostMapping("upload")
    public FileResponse profileImage(
            @ModelAttribute @Valid List<MultipartFile> file) throws UnknownHostException {
        return fileHandlingService.postProfileImage(file);
    }
}
