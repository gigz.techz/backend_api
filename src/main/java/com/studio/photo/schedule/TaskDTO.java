package com.studio.photo.schedule;

import com.studio.photo.entity.Task;
import com.studio.photo.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO {

    private Integer id;
    private String title;
    private String description;
    private LocalDateTime dueDate;
    private Date taskCreatedDate;
    private Set<ParticipantDTO> participants;


    public static TaskDTO fromEntity(Task task) {
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setTitle(task.getTitle());
        taskDTO.setDescription(taskDTO.description);
        taskDTO.setDueDate(task.getDueDate());
        taskDTO.setTaskCreatedDate(task.getTaskCreatedDate());
        Set<ParticipantDTO> participantDTOs = task.getParticipants().stream()
                .map(user -> new ParticipantDTO(user.getId(), user.getUsername()))
                .collect(Collectors.toSet());

        taskDTO.setParticipants(participantDTOs);
        return taskDTO;
    }
}

