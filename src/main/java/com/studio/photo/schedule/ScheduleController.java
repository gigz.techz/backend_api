package com.studio.photo.schedule;

import com.studio.photo.repeat.Response;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/protected/schedule/")
@RequiredArgsConstructor
public class ScheduleController {
    private final ScheduleService scheduleService;

    @PostMapping("task/create")
    public ResponseEntity<Response> createTask(@Valid @RequestBody TaskRequest taskRequest) {
        Response response = scheduleService.createTask(taskRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("task-fetch")
    public ResponseEntity<TaskListResponse> getAllTasksByCurrentUser(@RequestParam(defaultValue = "0") int page,
                                                                  @RequestParam(defaultValue = "10") int size) {
        TaskListResponse response = scheduleService.getAllTasksByCurrentUser(PageRequest.of(page,size));
        response.setData("success");
        if(response.getTasks().isEmpty())
        {
            response.setData("failed");

        }
        return ResponseEntity.ok(response);
    }


    @PostMapping("tasks")
    public ResponseEntity<TaskListResponse> getTasksByDateRange(
            @RequestBody @Valid TaskDateRangeRequest taskDateRangeRequest,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        TaskListResponse response = scheduleService.getTasksByDateRange(taskDateRangeRequest, PageRequest.of(page,size));
        response.setData("success");
        if(response.getTasks().isEmpty())
        {
            response.setData("failed");

        }
        return ResponseEntity.ok(response);
    }

}