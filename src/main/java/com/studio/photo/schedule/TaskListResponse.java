package com.studio.photo.schedule;

import lombok.Data;

import java.util.List;

@Data
public class TaskListResponse {
    private String data;
    private List<TaskDTO> tasks;

    public TaskListResponse(List<TaskDTO> tasks) {
        this.tasks = tasks;
    }
}

