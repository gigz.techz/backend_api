package com.studio.photo.schedule;

import com.studio.photo.dto.UserDTO;
import com.studio.photo.entity.Task;
import com.studio.photo.entity.User;
import com.studio.photo.notification.NotificationRequest;
import com.studio.photo.notification.NotificationService;
import com.studio.photo.repeat.Response;
import com.studio.photo.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ScheduleService {
    private final UserDTO userDTO;
    private final TaskRepository taskRepository;
    private final NotificationService notificationService;
    private boolean isSchedulerEnabled = true;

    public Response createTask(TaskRequest taskRequest) {
        try {
            User assignedUser = userDTO.getCurrentLoginUser();
            List<User> participants = userDTO.getUsersByIds(taskRequest.getParticipantIds());

            Task task = Task.builder()
                    .user(assignedUser)
                    .title(taskRequest.getTitle())
                    .description(taskRequest.getDescription())
                    .dueDate(taskRequest.getDueDate())
                    .participants(new HashSet<>(participants))
                    .build();

            taskRepository.save(task);

            LocalDateTime now = LocalDateTime.now();
            LocalDateTime twentyFourHoursFromNow = now.plusHours(24);
            LocalDateTime dueDateLocalDateTime = task.getDueDate();
            System.out.println("now "+now);
            System.out.println("twentyFourHoursFromNow "+twentyFourHoursFromNow);
            System.out.println("dueDateLocalDateTime "+dueDateLocalDateTime);
            if (dueDateLocalDateTime.isBefore(twentyFourHoursFromNow) || dueDateLocalDateTime.isEqual(twentyFourHoursFromNow)) {
                List<Integer> userIds = new ArrayList<>();
                userIds.add(assignedUser.getId());
                participants.forEach(participant -> userIds.add(participant.getId()));

                NotificationRequest notificationRequest = NotificationRequest.builder()
                        .userIds(userIds)
                        .message("Task due in the next 24 hours: " + task.getTitle())
                        .build();
                notificationService.pushNotify(notificationRequest);
            }
            return new Response("Task created successfully");
        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    public TaskListResponse getAllTasksByCurrentUser(Pageable pageable) {
        try {
            User currentUser = userDTO.getCurrentLoginUser();
            List<TaskDTO> taskList = taskRepository.findByUserOrderByDueDateAsc(currentUser, pageable)
                    .stream()
                    .map(TaskDTO::fromEntity)
                    .collect(Collectors.toList());

            return new TaskListResponse(taskList);

        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    public TaskListResponse getTasksByDateRange(TaskDateRangeRequest taskDateRangeRequest, Pageable pageable) {
        try{
            User currentUser = userDTO.getCurrentLoginUser();
            List<TaskDTO> taskList = taskRepository.findTaskAndMonthAndYearOrderByDueDateAsc(currentUser, taskDateRangeRequest.getMonth()+1,
                            taskDateRangeRequest.getYear(), pageable)
                    .stream()
                    .map(TaskDTO::fromEntity)
                    .collect(Collectors.toList());

            return new TaskListResponse(taskList);

        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void checkTaskDueDates() {
        if (!isSchedulerEnabled) {
            return;
        }

        try {
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime twentyFourHoursFromNow = now.plusHours(24);
            LocalDateTime fortyEightHoursFromNow = now.plusHours(48);

            List<Task> tasksDueInNext24To48Hours = taskRepository.findTasksDueSoon(twentyFourHoursFromNow, fortyEightHoursFromNow);

            for (Task task : tasksDueInNext24To48Hours) {
                User user = task.getUser();
                String message = "Tasks due in the next 24 to 48 hours " + task.getTitle();
                boolean notificationExists = notificationService.doesNotificationExist(user.getId(), message);

                if (!notificationExists) {
                    NotificationRequest notificationRequest = NotificationRequest.builder()
                            .userIds(Collections.singletonList(user.getId()))
                            .message(message)
                            .build();

                    notificationService.pushNotify(notificationRequest);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException("Scheduler exception " + Arrays.toString(e.getStackTrace()));
        }
    }

    public void setSchedulerEnabled(boolean schedulerEnabled) {
        this.isSchedulerEnabled = schedulerEnabled;
    }
}
