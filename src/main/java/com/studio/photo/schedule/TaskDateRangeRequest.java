package com.studio.photo.schedule;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public class TaskDateRangeRequest {
   private   int month;
   private   int year;
}
