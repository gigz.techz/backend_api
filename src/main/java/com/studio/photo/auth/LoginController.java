package com.studio.photo.auth;

import com.studio.photo.entity.User;
import com.studio.photo.user.UserPasswordRequest;
import com.studio.photo.user.UserPasswordResponse;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/public/")
@RequiredArgsConstructor
public class LoginController {
    private final LoginService loginService;
    @PostMapping("login")
    public LoginResponse login(
            @RequestBody @Valid LoginRequest loginRequest,
            @NonNull HttpServletResponse response) {
        return loginService.login(loginRequest);
    }
    @PostMapping("forget")
    public ForgetResponse sendOtp(
            @RequestBody @Valid LoginRequest loginRequest,
            @NonNull HttpServletResponse response) {
        return loginService.sendOtp(loginRequest);
    }
    @PostMapping("verify-otp")
    public VerifyResponse verifyOTP(
            @RequestBody @Valid LoginRequest loginRequest,
            @NonNull HttpServletResponse response
    )
    {
        return loginService.verifyOTP(loginRequest);
    }
}