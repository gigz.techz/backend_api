package com.studio.photo.auth;

import com.studio.photo.customEnum.UserRole;
import com.studio.photo.entity.SecureID;
import com.studio.photo.entity.User;
import com.studio.photo.exception.BadCredentialsException;
import com.studio.photo.repository.SecureIDRepository;
import com.studio.photo.repository.UserRepository;
import com.studio.photo.security.AppProperties;
import com.studio.photo.security.JwtService;
import com.studio.photo.user.UserPasswordRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.time.Instant;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class LoginService {
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final AppProperties appProperties;
    private SecureID secureID;
    private final SecureIDRepository secureIDRepository;

    public LoginResponse login(LoginRequest request) {
        try {
            String username = request.getUsername();
            String password = request.getPassword();

            Authentication authentication = new UsernamePasswordAuthenticationToken(
                    username,
                    password
            );
            authentication = authenticationManager.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User user = (User) authentication.getPrincipal();

            if (user.getRole() == UserRole.CUSTOMER) {
                boolean isAccountActive = user.isActive();
                String bearerToken = jwtService.generateToken(user);
                String refreshToken = jwtService.generateToken(user.getId());
                Instant refreshTokenExpireTime = jwtService.getExpirationTimeFromToken(refreshToken);
                Instant tokenExpirationTime = jwtService.getExpirationTimeFromToken(bearerToken);

                return LoginResponse.builder()
                        .token(bearerToken)
                        .refreshToken(refreshToken)
                        .refreshTokenExpireTime(refreshTokenExpireTime)
                        .tokenExpirationTime(tokenExpirationTime)
                        .isAccountActive(isAccountActive)
                        .build();
            } else {
                boolean isAccountActive = user.isActive();
                String bearerToken = jwtService.generateToken(user);
                String refreshToken = jwtService.generateToken(user.getId());
                Instant refreshTokenExpireTime = jwtService.getExpirationTimeFromToken(refreshToken);
                Instant tokenExpirationTime = jwtService.getExpirationTimeFromToken(bearerToken);

                return LoginResponse.builder()
                        .token(bearerToken)
                        .refreshToken(refreshToken)
                        .refreshTokenExpireTime(refreshTokenExpireTime)
                        .tokenExpirationTime(tokenExpirationTime)
                        .isAccountActive(isAccountActive)
                        .build();
            }

        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("Invalid username or password.");
        } catch (Exception e) {
            throw new RuntimeException("Error caught inside login: " + e.getLocalizedMessage());
        }
    }
    public ForgetResponse sendOtp(LoginRequest loginRequest) {
        try
        {
            var user= userRepository.findByUsername(loginRequest.getUsername()).orElseThrow();
            String otp= secureID.generateFourDigitOTP();
            LocalDateTime expiryDateTime = LocalDateTime.now().plusMinutes(appProperties.getOtpExpireDuration());
            secureIDRepository.deleteByUser(user);
            var sessionID=SecureID.builder()
                    .otp(otp)
                    .user(user)
                    .expiryDate(expiryDateTime)
                    .build();
            secureIDRepository.save(sessionID);
            return ForgetResponse.builder()
                    .status(true)
                    .build();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    public VerifyResponse verifyOTP(LoginRequest loginRequest) {
        try
        {
            var user= userRepository.findByUsername(loginRequest.getUsername()).orElseThrow();
            var secureId= secureIDRepository.findByUserAndOtp(user, loginRequest.getOtp()).orElseThrow();
            boolean isValid= secureID.isExpired();
            if(isValid)
            {
                throw new RuntimeException("otp expired:");
            }
            secureIDRepository.deleteByUser(user);
            String uniqueSid = new SecureID().generateUniqueSid();
            LocalDateTime expiryDateTime = LocalDateTime.now().plusMinutes(appProperties.getOtpExpireDuration());
            var secureID = SecureID.builder()
                    .user(user)
                    .sid(uniqueSid)
                    .expiryDate(expiryDateTime)
                    .build();
            secureIDRepository.save(secureID);
            String bearerToken = jwtService.generateToken(user);
            return VerifyResponse.builder()
                    .status(true)
                    .token(bearerToken)
                    .build();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
}
