package com.studio.photo.repository;

import com.studio.photo.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ServiceRepository extends JpaRepository<Service,Integer> {
    @Query("select s from Service s where s.serviceName = ?1")
    List<Service> findByServiceName(String serviceName);

}
