package com.studio.photo.repository;

import com.studio.photo.entity.Biography;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BiographyRepository extends JpaRepository<Biography,Integer> {
}
