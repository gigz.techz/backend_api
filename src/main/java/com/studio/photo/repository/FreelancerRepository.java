package com.studio.photo.repository;

import com.studio.photo.entity.Freelancer;
import com.studio.photo.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface FreelancerRepository extends JpaRepository<Freelancer,Integer> {

    @Query("select f from Freelancer f where upper(f.fullName) = upper(?1)")
    Page<Freelancer> findByFullNameAllIgnoreCase(String fullName, Pageable pageable);

    @Query("select f from Freelancer f where f.user = ?1")
    Optional<Freelancer> findByUser(User user);
}
