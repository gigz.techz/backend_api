package com.studio.photo.repository;

import com.studio.photo.entity.GroupChat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupChatTSRepository extends JpaRepository<GroupChat,Integer> {
}
