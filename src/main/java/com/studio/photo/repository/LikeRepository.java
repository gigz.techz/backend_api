package com.studio.photo.repository;

import com.studio.photo.entity.Like;
import com.studio.photo.entity.Post;
import com.studio.photo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface LikeRepository extends JpaRepository<Like,Integer> {

    @Query("select (count(l) > 0) from Like l where l.user.id = ?1 and l.post.id = ?2")
    boolean existsByUser_IdAndPost_Id(Integer id, Integer id1);
}
