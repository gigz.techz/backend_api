package com.studio.photo.repository;

import com.jayway.jsonpath.JsonPath;
import com.studio.photo.entity.ChatMessage;
import com.studio.photo.entity.GroupChat;
import com.studio.photo.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessage,Integer>{
    ChatMessage findByGroupChat_Id(Integer id);

    @Query("select c from ChatMessage c where c.sender = ?1 or c.receiver = ?2")
    Page<ChatMessage> findBySenderOrReceiver(User sender, User receiver, Pageable pageable);


    @Query("select c from ChatMessage c where c.groupChat = ?1 order by c.timestamp")
    Page<ChatMessage> findByGroupChatOrderByTimestampAsc(GroupChat groupChat, Pageable pageable);

    @Query("select c from ChatMessage c where c.receiver = ?1 and c.sender = ?2 order by c.timestamp")
    Page<ChatMessage> findByReceiverAndSenderOrderByTimestampAsc(User receiver, User sender, Pageable pageable);

//    @Query("select c from ChatMessage c where (c.sender = ?1 and c.receiver = ?2) or (c.sender = ?2 and c.receiver = ?1) order by c.timestamp")
//    Page<ChatMessage> findBySenderAndReceiverOrSenderAndReceiverOrderByTimestampAsc(User user1, User user2, User user3, User user4, Pageable pageable);
@Query("select c from ChatMessage c where " +
        "((c.sender = ?1 and c.receiver = ?2) or (c.sender = ?2 and c.receiver = ?1)) " +
        "and c.groupChat is null " +
        "order by c.timestamp")
Page<ChatMessage> findBySenderAndReceiverOrSenderAndReceiverOrderByTimestampAsc(
        User user1, User user2, User user3, User user4, Pageable pageable);

}