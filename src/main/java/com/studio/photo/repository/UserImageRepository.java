package com.studio.photo.repository;

import com.studio.photo.entity.User;
import com.studio.photo.entity.UserImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserImageRepository extends JpaRepository<UserImage, Integer> {
    @Query("select u from UserImage u where u.user = ?1")
    Page<UserImage> findByUser(User user, Pageable pageable);

    @Query("select u from UserImage u where u.id = ?1 and u.user = ?2")
    Optional<UserImage> findByIdAndUser(Integer id, User user);

}
