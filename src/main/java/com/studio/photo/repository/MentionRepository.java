package com.studio.photo.repository;

import com.studio.photo.entity.Mention;
import com.studio.photo.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface MentionRepository extends JpaRepository<Mention,Integer> {

}
