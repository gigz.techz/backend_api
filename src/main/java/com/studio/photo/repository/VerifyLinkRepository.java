package com.studio.photo.repository;

import com.studio.photo.entity.User;
import com.studio.photo.entity.VerifyLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface VerifyLinkRepository extends JpaRepository<VerifyLink,Integer> {
    @Query("select (count(v) > 0) from VerifyLink v where v.user = ?1 and v.verifyToken = ?2")
    boolean existsByUserAndVerifyToken(User user, String verifyToken);

    @Transactional
    @Modifying
    @Query("delete from VerifyLink v where v.user = ?1")
    int deleteByUser(User user);


}
