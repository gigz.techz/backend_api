package com.studio.photo.repository;

import com.studio.photo.entity.UserServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserServicesRepository extends JpaRepository<UserServices,Integer> {
}
