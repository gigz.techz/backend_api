package com.studio.photo.repository;

import com.studio.photo.entity.ChatMessage;
import com.studio.photo.entity.User;
import com.studio.photo.entity.UsersChatTS;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserChatTSRepository extends JpaRepository<UsersChatTS,Integer> {
    Optional<UsersChatTS> findByUserAndChatMessage(User user, ChatMessage chatMessage);

    @Query("select u from UsersChatTS u where u.chatMessage = ?1 and u.user = ?2")
    Optional<UsersChatTS> findByChatMessageAndUser(ChatMessage chatMessage, User user);
}
