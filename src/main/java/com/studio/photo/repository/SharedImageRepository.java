package com.studio.photo.repository;

import com.studio.photo.entity.SharedImages;
import com.studio.photo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SharedImageRepository extends JpaRepository<SharedImages,Integer> {
    @Query("select (count(s) > 0) from SharedImages s where s.sharedWithUser = ?1 and s.sharedByUser = ?2")
    boolean existsBySharedWithUserAndSharedByUser(User sharedWithUser, User sharedByUser);
}
