package com.studio.photo.repository;

import com.studio.photo.customEnum.UserRole;
import com.studio.photo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    @Query("select u from User u where u.username = ?1")
    Optional<User> findByUsername(String username);

    @Transactional
    @Modifying
    @Query("update User u set u.password = ?1, u.active = ?2 where u.username = ?3")
    int updatePasswordAndActiveByUsername(String password, boolean active, String username);


    @Query("select (count(u) > 0) from User u where u.username = ?1 and u.role = ?2 and u.accountVerified = ?3")
    boolean existsByUsernameAndRoleAndAccountVerified(String username, UserRole role, boolean accountVerified);

    boolean existsByUsername(String username);
}
