package com.studio.photo.repository;

import com.studio.photo.entity.Biography;
import com.studio.photo.entity.SocialMediaLink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SocialMediaLinkRepository extends JpaRepository<SocialMediaLink,Integer> {
    @Query("""
            select s from SocialMediaLink s inner join s.biography.socialMediaLinks socialMediaLinks
            where socialMediaLinks.id = ?1""")
    List<SocialMediaLink> findByBiography_SocialMediaLinks_Id(Integer id);

    @Transactional
    @Modifying
    @Query("delete from SocialMediaLink s where s.biography = ?1")
    int deleteByBiography(Biography biography);
}
