package com.studio.photo.repository;

import com.studio.photo.entity.StudioServices;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserServiceRepository extends JpaRepository<StudioServices,Integer> {
}
