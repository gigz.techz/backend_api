package com.studio.photo.repository;

import com.studio.photo.entity.SecureID;
import com.studio.photo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface SecureIDRepository extends JpaRepository<SecureID,Integer> {
    @Query("select s from SecureID s where s.user = ?1")
    Optional<SecureID> findByUser(User user);

    @Transactional
    @Modifying
    @Query("delete from SecureID s where s.user = ?1")
    int deleteByUser(User user);

    @Query("select s from SecureID s where s.user = ?1 and s.otp = ?2")
    Optional<SecureID> findByUserAndOtp(User user, String otp);
}
