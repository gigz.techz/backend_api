package com.studio.photo.repository;

import com.studio.photo.entity.Notification;
import com.studio.photo.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification,Integer> {
    @Query("select n from Notification n where n.user = ?1 and n.read = ?2")
    Page<Notification> findByUserAndRead(User user, boolean read, Pageable pageable);

    @Query("select n from Notification n where n.id in :notificationIds and n.read = :read")
    List<Notification> findByNotificationIdsAndRead(@Param("notificationIds") List<Integer> notificationIds, @Param("read") boolean read);

    boolean existsByUser_IdAndMessageAndReadAllIgnoreCase(Integer id, String message, boolean read);
}
