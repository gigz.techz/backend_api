package com.studio.photo.repository;

import com.studio.photo.entity.Studio;
import com.studio.photo.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface StudioRepository extends JpaRepository<Studio,Integer> {

    @Query("select s from Studio s where upper(s.studioName) = upper(?1)")
    Page<Studio> findByStudioNameAllIgnoreCase(String studioName, Pageable pageable);

    @Query("select s from Studio s where s.user = ?1")
    Optional<Studio> findByUser(User user);
}
