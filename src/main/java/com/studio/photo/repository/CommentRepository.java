package com.studio.photo.repository;

import com.studio.photo.entity.Comment;
import com.studio.photo.entity.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Integer> {
    List<Comment> findByPostAndParentCommentIsNull(Post post);

    @Query("select c from Comment c where c.parentComment = ?1")
    List<Comment> findByParentComment(Comment parentComment, Pageable pageable);

    @Query("select (count(c) > 0) from Comment c where c.user.id = ?1 and c.post.id = ?2")
    boolean existsByUser_IdAndPost_Id(Integer id, Integer id1);
}
