package com.studio.photo.repository;

import com.studio.photo.entity.GroupChat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupChatRepository extends JpaRepository<GroupChat,Integer> {
    @Query("select g from GroupChat g inner join g.participants participants where participants.id = ?1")
    Page<GroupChat> findByParticipantsContaining(Integer id, Pageable pageable);
}
