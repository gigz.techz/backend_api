package com.studio.photo.repository;

import com.studio.photo.entity.Post;
import com.studio.photo.entity.User;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface PostRepository extends JpaRepository<Post,Integer> {
    @Query("select p from Post p where p.userId = ?1")
    Page<Post> findByUserId(User userId, Pageable pageable);

    @Query("select p from Post p where p.id = ?1 and p.userId = ?2")
    Optional<Post> findByIdAndUserId(Integer id, User userId);

    @Modifying
    @Query("DELETE FROM Post p WHERE p.id = :postId")
    void deletePostAndRelatedEntities(@Param("postId") Integer postId);

    Page<Post> findByIdIn(List<Integer> randomIndices, Pageable page);

    @Override
    void deleteById(@NonNull Integer integer);
}
