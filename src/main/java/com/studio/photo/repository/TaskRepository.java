package com.studio.photo.repository;

import com.studio.photo.entity.Task;
import com.studio.photo.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task,Integer> {
    @Query("select t from Task t where t.user = ?1 order by t.dueDate asc")
    Page<Task> findByUserOrderByDueDateAsc(User user, Pageable pageable);
    @Query("SELECT t FROM Task t WHERE t.user = ?1 AND MONTH(t.dueDate) = ?2 AND YEAR(t.dueDate) = ?3 ORDER BY t.dueDate ASC")
    Page<Task> findTaskAndMonthAndYearOrderByDueDateAsc(User user, int month, int year, Pageable pageable);

    @Query("SELECT t FROM Task t WHERE t.dueDate BETWEEN :startDate AND :endDate")
    List<Task> findTasksDueSoon(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

}
