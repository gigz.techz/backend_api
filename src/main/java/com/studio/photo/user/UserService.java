package com.studio.photo.user;

import com.studio.photo.customEnum.UserRole;
import com.studio.photo.dto.UserDTO;
import com.studio.photo.entity.*;
import com.studio.photo.repeat.Response;
import com.studio.photo.repository.*;
import com.studio.photo.security.AppProperties;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserService {
    private final SecureIDRepository secureIDRepository;
    private final UserRepository userRepository;
    private final AppProperties appProperties;
    private final SocialMediaLinkRepository socialMediaLinkRepository;
    @PersistenceContext
    private EntityManager entityManager;
    private final BiographyRepository biographyRepository;
    private final UserDTO userDTO;
    private final UserImageRepository userImageRepository;
    private final SharedImageRepository sharedImageRepository;

    public UserProfileResponse getProfile(User user , String baseUrl, boolean isSame) {
        try
        {
            String username= user.getUsername();
            String fullName=null;
            String studioName = null;
            String ownerName = null;
            String governmentRegistrationId = null;
            String mobileNumber = null;
            String address = null;
            String city = null;
            String profileImageName =null;

            if(user.getRole().equals(UserRole.CUSTOMER))
            {
                fullName=user.getCustomer().getFullName();
                address=user.getCustomer().getAddress();
                city=user.getCustomer().getCity();
                profileImageName=user.getCustomer().getProfilePicturePath();
            }
            else if(user.getRole().equals(UserRole.STUDIO))
            {
                ownerName=user.getStudio().getOwnerName();
                studioName=user.getStudio().getStudioName();
                address=user.getStudio().getAddress();
                city=user.getStudio().getCity();
                governmentRegistrationId=user.getStudio().getGovernmentRegistrationId();
                mobileNumber=user.getStudio().getMobileNumber();
                profileImageName=user.getStudio().getProfilePicturePath();
            }
            else
            {
                fullName=user.getFreelancer().getFullName();
                address=user.getFreelancer().getAddress();
                city=user.getFreelancer().getCity();
                mobileNumber=user.getFreelancer().getMobileNumber();
                profileImageName=user.getFreelancer().getProfilePicturePath();

            }

            Biography biography = user.getBiography();
            String description = null;
            long postCount = 0;
            long clubCount = 0;
            List<SocialMediaDto> socialMediaLinks = null;

            if (biography != null) {
                description = biography.getDescription();
                postCount = biography.getPostCount();
                socialMediaLinks = convertSocialMediaLinksToDto(biography.getSocialMediaLinks());
            }
            boolean flag=false;
            if(!isSame)
            {
                User currentUser = userDTO.getCurrentLoginUser();
                flag= sharedImageRepository.existsBySharedWithUserAndSharedByUser(currentUser, userDTO.getUserById(user.getId()));
            }
            return UserProfileResponse.builder()
                    .profileImage(profileImageName)
                    .governmentRegistrationId(governmentRegistrationId)
                    .address(address)
                    .city(city)
                    .username(username)
                    .fullName(fullName)
                    .studioName(studioName)
                    .ownerName(ownerName)
                    .mobileNumber(mobileNumber)
                    .description(description)
                    .postCount(postCount)
                    .clubCount(clubCount)
                    .socialMediaLinks(socialMediaLinks)
                    .isShared(flag)
                    .build();

        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    private List<SocialMediaDto> convertSocialMediaLinksToDto(List<SocialMediaLink> socialMediaLinks) {
        List<SocialMediaDto> socialMediaDtoList = new ArrayList<>();
        if (socialMediaLinks != null) {
            for (SocialMediaLink socialMediaLink : socialMediaLinks) {
                SocialMediaDto socialMediaDto = SocialMediaDto.builder()
                        .platformName(socialMediaLink.getPlatformName())
                        .path(socialMediaLink.getPath())
                        .build();
                socialMediaDtoList.add(socialMediaDto);
            }
        }
        return socialMediaDtoList;
    }

    public UserPasswordResponse verifyCurrentPassword(User user, UserPasswordRequest userPasswordRequest) {
        try
        {
            boolean isPasswordValid= user.matchesPassword(userPasswordRequest.getPassword());
            if (!isPasswordValid) {
                throw new RuntimeException("password do not match");
            }

            secureIDRepository.deleteByUser(user);
            String uniqueSid = new SecureID().generateUniqueSid();
            LocalDateTime expiryDateTime = LocalDateTime.now().plusMinutes(appProperties.getOtpExpireDuration());

            var secureID = SecureID.builder()
                    .user(user)
                    .sid(uniqueSid)
                    .expiryDate(expiryDateTime)
                    .build();
            secureIDRepository.save(secureID);
            return UserPasswordResponse.builder()
                    .status(true)
                    .build();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    public UserPasswordResponse changePassword(User user, UserPasswordRequest userPasswordRequest) {
        try
        {
            if(user.isActive())
            {
               boolean checkPasswordMatch= new User().matchesPassword(userPasswordRequest.getCurrentPassword());
               if(checkPasswordMatch)
               {
                   user.setPassword(new User().getHashPassword(userPasswordRequest.getNewPassword()));
               }
               else {
                   throw new RuntimeException("Password did not match");
               }

            }
            else{
                user.setPassword(new User().getHashPassword(userPasswordRequest.getNewPassword()));
                user.setActive(true);
            }
            userRepository.save(user);
            return UserPasswordResponse.builder()
                    .status(true)
                    .build();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    @Transactional
    public Response updateProfile(UserProfileRequest userProfileRequest) {
        User user = userDTO.getCurrentLoginUser();

        if (user.getRole().equals(UserRole.STUDIO)) {
            Studio studio = user.getStudio();
            updateStudioProfile(studio, userProfileRequest);
        } else if (user.getRole().equals(UserRole.FREELANCER)) {
            Freelancer freelancer = user.getFreelancer();
            updateFreelancerProfile(freelancer, userProfileRequest);
        } else {
            Customer customer = user.getCustomer();
            updateCustomerProfile(customer, userProfileRequest);
        }

        updateBiographyAndSocialMediaLinks(user, userProfileRequest.getDescription());
        User updatedUser = userRepository.save(user);

        updateSocialMediaLinks(updatedUser.getBiography().getId(), userProfileRequest.getSocialMediaLinks());

        return new Response("Profile updated successfully");
    }

    private void updateStudioProfile(Studio studio, UserProfileRequest userProfileRequest) {
        if (userProfileRequest.getStudioName() != null) {
            studio.setStudioName(userProfileRequest.getStudioName());
        }
        if (userProfileRequest.getOwnerName() != null) {
            studio.setOwnerName(userProfileRequest.getOwnerName());
        }
        if (userProfileRequest.getGovernmentRegistrationId() != null) {
            studio.setGovernmentRegistrationId(userProfileRequest.getGovernmentRegistrationId());
        }
    }

    private void updateFreelancerProfile(Freelancer freelancer, UserProfileRequest userProfileRequest) {
        if (userProfileRequest.getFullName() != null) {
            freelancer.setFullName(userProfileRequest.getFullName());
        }
        if (userProfileRequest.getAddress() != null) {
            freelancer.setAddress(userProfileRequest.getAddress());
        }
        if (userProfileRequest.getCity() != null) {
            freelancer.setCity(userProfileRequest.getCity());
        }
        if (userProfileRequest.getMobileNumber() != null) {
            freelancer.setMobileNumber(userProfileRequest.getMobileNumber());
        }
        if (userProfileRequest.getProfilePicturePath() != null) {
            freelancer.setProfilePicturePath(userProfileRequest.getProfilePicturePath());
        }
    }

    private void updateCustomerProfile(Customer customer, UserProfileRequest userProfileRequest) {
        if (userProfileRequest.getFullName() != null) {
            customer.setFullName(userProfileRequest.getFullName());
        }
        if (userProfileRequest.getAddress() != null) {
            customer.setAddress(userProfileRequest.getAddress());
        }
        if (userProfileRequest.getCity() != null) {
            customer.setCity(userProfileRequest.getCity());
        }
        if (userProfileRequest.getProfilePicturePath() != null) {
            customer.setProfilePicturePath(userProfileRequest.getProfilePicturePath());
        }
    }

    private void updateBiographyAndSocialMediaLinks(User user, String description) {
        if (user.getBiography() == null) {
            user.setBiography(new Biography());
        }
        Biography biography = user.getBiography();
        biography.setDescription(description);
    }

    private void updateSocialMediaLinks(Integer bioId, List<SocialMediaDto> socialMediaLinks) {
        User user = userDTO.getCurrentLoginUser();
        Biography biography = user.getBiography();
        socialMediaLinkRepository.deleteByBiography(biography);

        if (socialMediaLinks != null && !socialMediaLinks.isEmpty()) {
            Set<String> existingPlatforms = new HashSet<>();

            for (SocialMediaDto socialMediaDto : socialMediaLinks) {
                String platformName = socialMediaDto.getPlatformName().toLowerCase();
                if (!existingPlatforms.contains(platformName)) {
                    SocialMediaLink socialMediaLink = new SocialMediaLink();
                    socialMediaLink.setPlatformName(platformName);
                    socialMediaLink.setPath(socialMediaDto.getPath());
                    socialMediaLink.setBiography(biography);
                    biography.getSocialMediaLinks().add(socialMediaLink);

                    existingPlatforms.add(platformName);
                }
            }
        }

        biographyRepository.save(biography);
    }

    public UserResponse getRoleAndId() {
        return UserResponse.builder()
                .message(String.valueOf(userDTO.getCurrentLoginUser().getRole()))
                .userId(userDTO.getCurrentLoginUser().getId())
                .build();
    }

    public void uploadPhotos(List<String> photosPath) {
        User user = userDTO.getCurrentLoginUser();
            for (String photoPath : photosPath) {
                UserImage userImage = new UserImage();
                userImage.setImagePath(photoPath);
                userImage.setUser(user);
                user.getUserImages().add(userImage);
            }
            userRepository.save(user);

    }

    public List<PhotoInfo> fetchPhotos(Pageable pageable) {
        User user = userDTO.getCurrentLoginUser();
        Page<UserImage> page = userImageRepository.findByUser(user, pageable);

        return page.getContent().stream()
                .map(userImage -> new PhotoInfo(userImage.getId(), userImage.getImagePath()))
                .collect(Collectors.toList());
    }

    public void sharePhotos(PhotoUploadRequest photoUploadRequest) {
        User currentUser = userDTO.getCurrentLoginUser();
        List<Integer> sharedUserIds = photoUploadRequest.getUserId();

                for (Integer sharedUserId : sharedUserIds) {
                    User sharedWithUser = userRepository.findById(sharedUserId).orElse(null);
                    if (sharedWithUser != null) {
                        SharedImages sharedImages = new SharedImages();
                        sharedImages.setSharedByUser(currentUser);
                        sharedImages.setSharedWithUser(sharedWithUser);
                        sharedImageRepository.save(sharedImages);
                    }
                }


    }

    public List<PhotoInfo> fetchPhotosByUserId(Integer userId, Pageable pageable) {
        User currentUser = userDTO.getCurrentLoginUser();
        boolean hasPermission = sharedImageRepository.existsBySharedWithUserAndSharedByUser(currentUser, userDTO.getUserById(userId));
        if (hasPermission) {
            User user =userDTO.getUserById(userId);
            if (user != null) {
                Page<UserImage> page = userImageRepository.findByUser(user, pageable);

                return page.getContent().stream()
                        .map(userImage -> new PhotoInfo(userImage.getId(), userImage.getImagePath()))
                        .collect(Collectors.toList());
            }
        }
        return Collections.emptyList();
    }

    public List<ServiceDTO> fetchUserServices() {
        User user = userDTO.getCurrentLoginUser();

        if (user.getRole().equals(UserRole.CUSTOMER)) {
            throw new RuntimeException("Unauthorized");
        }

        return user.getUserServices()
                .stream()
                .map(service -> ServiceDTO.builder()
                        .id(service.getId())
                        .serviceName(service.getServiceName())
                        .build())
                .collect(Collectors.toList());
    }
}