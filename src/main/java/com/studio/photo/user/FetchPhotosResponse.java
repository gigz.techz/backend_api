package com.studio.photo.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FetchPhotosResponse {
    private String message;
    private List<PhotoInfo> photoPaths;
}
