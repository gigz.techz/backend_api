package com.studio.photo.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileRequest {
    private String fullName;

    private String address;

    private String city;

    private String profilePicturePath;

    private String mobileNumber;

    private String studioName;

    private String ownerName;

    private String governmentRegistrationId;

    private String description;

    private List<SocialMediaDto> socialMediaLinks;


}
