package com.studio.photo.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PhotoUploadRequest {
    private List<String> photosPath;
    private List<Integer> userId;

}
