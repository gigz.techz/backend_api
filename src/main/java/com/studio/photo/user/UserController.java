package com.studio.photo.user;

import com.studio.photo.dto.UserDTO;
import com.studio.photo.entity.User;
import com.studio.photo.exception.AuthorizationException;
import com.studio.photo.repeat.Response;
import com.studio.photo.security.UserDetail;
import com.studio.photo.signup.SignUpController;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import software.amazon.awssdk.core.ResponseInputStream;

import java.util.List;


@RestController
@RequestMapping("/api/protected/")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserDetail userDetails;
    private final SignUpController signUpController;
    private final UserDTO userDTO;

    @GetMapping("profile")
    public UserProfileResponse profile(
            @NonNull HttpServletRequest httpServletRequest,
            @NonNull HttpServletResponse response) {
        User user = userDetails.getUser();
        String baseUrl = signUpController.getBaseUrl(httpServletRequest);
        return userService.getProfile(user , baseUrl,true);
    }

    @GetMapping("profile/{userId}")
    public UserProfileResponse getProfileByAnotherUser(
            @PathVariable("userId") Integer userId,
            @NonNull HttpServletRequest httpServletRequest) {
        User user = userDTO.getUserById(userId);
        String baseUrl = signUpController.getBaseUrl(httpServletRequest);
        return userService.getProfile(user , baseUrl,false);
    }

    @PutMapping("profile")
    public Response updateProfile(
            @RequestBody UserProfileRequest userProfileRequest
    )
    {
        return userService.updateProfile(userProfileRequest);
    }


    @PostMapping("verify-password")
    public UserPasswordResponse verifyCurrentPassword(
            @RequestBody @Valid UserPasswordRequest userPasswordRequest,
            @NonNull HttpServletResponse response
    )
    {
        User user = userDetails.getUser();
        return userService.verifyCurrentPassword(user,userPasswordRequest);
    }

    @PutMapping("change-password")
    public UserPasswordResponse changePassword(
            @RequestBody @Valid UserPasswordRequest userPasswordRequest,
            @NonNull HttpServletResponse response
    )
    {

        return userService.changePassword(userDTO.getCurrentLoginUser(),userPasswordRequest);
    }
    @GetMapping("role-and-id")
    public UserResponse profile() {
        return userService.getRoleAndId();
    }

    @PostMapping("/photo-uploads")
    public ResponseEntity<Response> uploadPhotos(@RequestBody PhotoUploadRequest request) {
            userService.uploadPhotos(request.getPhotosPath());

            return ResponseEntity.ok(Response.builder()
                            .message("success")
                    .build());
    }



    @GetMapping("/photo-uploads")
    public ResponseEntity<FetchPhotosResponse> fetchPhotos(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int pageSize) {

        List<PhotoInfo> photoPaths = userService.fetchPhotos(PageRequest.of(page, pageSize));
        FetchPhotosResponse response = new FetchPhotosResponse();
        response.setMessage("success");
        response.setPhotoPaths(photoPaths);
        return ResponseEntity.ok(response);
    }


    @GetMapping("/photo-uploads/{userId}")
    public ResponseEntity<FetchPhotosResponse> fetchPhotosByUserId(
            @PathVariable("userId") Integer userId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int pageSize) {

        List<PhotoInfo> photoPaths = userService.fetchPhotosByUserId(userId,PageRequest.of(page, pageSize));
        FetchPhotosResponse response = new FetchPhotosResponse();
        response.setMessage("success");
        response.setPhotoPaths(photoPaths);
        return ResponseEntity.ok(response);
    }



    @PostMapping("/share/media")
    public ResponseEntity<Response> sharePhotos(@RequestBody PhotoUploadRequest request) {
        userService.sharePhotos(request);
        return ResponseEntity.ok(Response.builder()
                .message("success")
                .build());
    }


    @GetMapping("/user-services")
    public ResponseEntity<UserServiceResponse> getUserServices() {
        try {
            List<ServiceDTO> userServices = userService.fetchUserServices();
            return ResponseEntity.ok(UserServiceResponse.builder()
                    .data("success")
                    .dtoList(userServices)
                    .build());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body(UserServiceResponse.builder()
                            .data("Unauthorized")
                            .build());
        }
    }






}