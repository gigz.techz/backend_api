package com.studio.photo.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileResponse {
    private String username;
    private String fullName;
    private String address;
    private String city;
    private String profileImage;
    private String studioName;
    private String governmentRegistrationId;
    private String mobileNumber;
    private String ownerName;
    private String description;
    private long postCount;
    private long clubCount;

    private List<SocialMediaDto> socialMediaLinks;

    private boolean isShared;
}
