package com.studio.photo.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostDTOResponse {
    private Integer id;
    private String name;
    private String profilePicPath;
}
