package com.studio.photo.dto;

import com.studio.photo.auth.LoginRequest;
import com.studio.photo.customEnum.UserRole;
import com.studio.photo.entity.Customer;
import com.studio.photo.entity.Freelancer;
import com.studio.photo.entity.Studio;
import com.studio.photo.entity.User;
import com.studio.photo.repository.CustomerRepository;
import com.studio.photo.repository.FreelancerRepository;
import com.studio.photo.repository.StudioRepository;
import com.studio.photo.repository.UserRepository;
import com.studio.photo.security.UserDetail;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserDTO {
    private final UserRepository userRepository;
    private final FreelancerRepository freelancerRepository;
    private final CustomerRepository customerRepository;
    private final StudioRepository studioRepository;
    private final AuthenticationManager authenticationManager;


    public User createUser(User user)
    {
        if (isUsernameValid(user.getUsername())) {
            throw new RuntimeException("Username '" + user.getUsername() + "' is already in use.");
        }
        user.setPassword(encodePassword(user.getPassword()));
        return userRepository.save(user);
    }

    private String encodePassword(String rawPassword)
    {
        return new BCryptPasswordEncoder().encode(rawPassword);
    }

    public boolean isUserValid(LoginRequest request) {
        String username = request.getUsername();
        String password = request.getPassword();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return true;
    }


    public  User getCurrentLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userRepository.
                findByUsername(userDetails.getUsername()).orElseThrow();
    }

    public User getUserByUserName(String username) {
        return userRepository
                .findByUsername(username)
                .orElseThrow(() -> new NoSuchElementException("User not found with username: " + username));
    }

    public User getUserById(Integer id) {
        return userRepository
                .findById(id)
                .orElseThrow(() -> new NoSuchElementException("User not found with id: " + id));
    }

    public List<User> getUsersByIds(List<Integer> userIds) {
        return userIds.stream()
                .map(this::getUserById)
                .collect(Collectors.toList());
    }

    public boolean isUsernameValid(String username)
    {
        return userRepository.existsByUsername(username);
    }

    public UserRole getUserRole()
    {
        return getCurrentLoginUser().getRole();
    }

    public Freelancer getFreeLancerByUser(User user)
    {
        return freelancerRepository.findByUser(user).orElseThrow();
    }
    public Studio getStudioByUser(User user)
    {
        return studioRepository.findByUser(user).orElseThrow();
    }

    public Customer getCustomerByUser(User user)
    {
        return customerRepository.findByUser(user).orElseThrow();
    }

    public PostDTOResponse getUserIdAndNameByUserId(User user)
    {
        UserRole userRole = user.getRole();

        if(userRole.equals(UserRole.FREELANCER))
        {
            var freelancer = getFreeLancerByUser(user);
            return PostDTOResponse.builder()
                    .name(freelancer.getFullName())
                    .profilePicPath(freelancer.getProfilePicturePath())
                    .id(user.getId())
                    .build();
        } else if (userRole.equals(UserRole.STUDIO)) {
                var studio = getStudioByUser(user);
            return PostDTOResponse.builder()
                    .name(studio.getStudioName())
                    .profilePicPath(studio.getProfilePicturePath())
                    .id(user.getId())
                    .build();
        }
        else {
            var customer = getCustomerByUser(user);
            return PostDTOResponse.builder()
                    .name(customer.getFullName())
                    .profilePicPath(customer.getProfilePicturePath())
                    .id(user.getId())
                    .build();
        }
    }

    public String returnNameByUsername(String username)
    {
        User user = getUserByUserName(username);

        PostDTOResponse postDTOResponse = getUserIdAndNameByUserId(user);
        return postDTOResponse.getName();
    }



}
