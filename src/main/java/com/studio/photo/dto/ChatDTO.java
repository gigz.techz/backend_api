package com.studio.photo.dto;

import com.studio.photo.entity.ChatMessage;
import com.studio.photo.entity.GroupChat;
import com.studio.photo.repository.ChatMessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ChatDTO {
    private final ChatMessageRepository chatMessageRepository;

//    public GroupChat getGroupChatById(Integer groupChatId)
//    {
//       return chatMessageRepository.findByGroupChat_Id(groupChatId).getGroupChat();
//    }

    public GroupChat getGroupChatById(Integer groupChatId) {
        ChatMessage chatMessage = chatMessageRepository.findByGroupChat_Id(groupChatId);
        return chatMessage != null ? chatMessage.getGroupChat() : null;
    }

    public ChatMessageDTO mapToDTO(ChatMessage chatMessage) {
        return new ChatMessageDTO(
                chatMessage.getId(),
                chatMessage.getSender(),
                chatMessage.getReceiver(),
                chatMessage.getMessage(),
                chatMessage.getTimestamp(),
                chatMessage.getGroupChat()
        );
    }
}
