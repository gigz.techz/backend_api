package com.studio.photo.dto;

import com.studio.photo.entity.GroupChat;
import com.studio.photo.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatMessageDTO {
    private Integer id;
    private User sender;
    private User receiver;
    private String message;
    private LocalDateTime timestamp;
    private GroupChat groupChat;
}
