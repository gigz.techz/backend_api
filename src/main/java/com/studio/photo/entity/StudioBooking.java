package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;
import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudioBooking {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "studio_id")
    private Studio studio;


    @Temporal(TemporalType.TIMESTAMP)
    private Date bookingDate;

    @Column(name = "booking_amount")
    private Double bookingAmount;

    private String bookingDetails;

    @OneToMany(mappedBy = "studioBooking", cascade = CascadeType.ALL)
    private List<StudioServices> bookedServices;
}

