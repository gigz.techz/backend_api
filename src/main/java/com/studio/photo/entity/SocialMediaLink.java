package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SocialMediaLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String platformName;

    private String path;

    @ManyToOne
    @JoinColumn(name = "bio_id")
    private Biography biography;

}
