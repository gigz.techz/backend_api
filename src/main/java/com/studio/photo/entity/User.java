package com.studio.photo.entity;

import com.studio.photo.customEnum.UserRole;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.SecureRandom;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NonNull
    @Column(unique = true)
    private String username;

    @ColumnDefault("false")
    private boolean active;

    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_date", nullable = false, updatable = false)
    private Date createdDate;

    @OneToMany(mappedBy = "sender")
    private List<ChatMessage> sentMessages;

    @OneToMany(mappedBy = "receiver")
    private List<ChatMessage> receivedMessages;

    @ManyToMany(mappedBy = "participants")
    private List<GroupChat> groupChats;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "biography_id")
    private Biography biography;


    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserImage> userImages;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserServices> userServices;


    @PrePersist
    protected void onCreate() {
        createdDate = new Date();
    }

    @Enumerated(EnumType.STRING)
    private UserRole role;

    @ColumnDefault("false")
    private boolean accountVerified;

    @OneToOne(orphanRemoval = true)
    @JoinTable(name = "user_studio",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "studio_id"))
    private Studio studio;

    @OneToOne(orphanRemoval = true)
    @JoinTable(name = "user_customer",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private Customer customer;

    @OneToOne(orphanRemoval = true)
    @JoinTable(name = "user_freelancer",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "freelancer_id"))
    private Freelancer freelancer;




    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Set.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Transient
    private transient BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    public boolean matchesPassword(String providedPassword) {
        return passwordEncoder.matches(providedPassword, this.password);
    }
    private static final String PASSWORD_CHARACTERS = "0123456789";
    private static final int PASSWORD_LENGTH = 4;
    public  String getRandomPassword() {

        SecureRandom random = new SecureRandom();
        StringBuilder password = new StringBuilder();

        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            int randomIndex = random.nextInt(PASSWORD_CHARACTERS.length());
            password.append(PASSWORD_CHARACTERS.charAt(randomIndex));
        }

        return password.toString();
    }
    public String getHashPassword(String rawPassword)
    {
        return (passwordEncoder.encode(rawPassword));
    }

}
