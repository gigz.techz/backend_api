package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SecureID {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    private User user;

    private String sid;

    private String otp;

    private LocalDateTime expiryDate;

    public boolean isExpired() {
        return LocalDateTime.now().isAfter(expiryDate);
    }
    public String generateFourDigitOTP() {
        SecureRandom random = new SecureRandom();
        int otpValue = 1000 + random.nextInt(9000);
        return String.valueOf(otpValue);
    }
    public String generateUniqueSid() {
        return UUID.randomUUID().toString();
    }


}
