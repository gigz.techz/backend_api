package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudioServices {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "studio_id")
    private Studio studio;

    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    private double serviceAmount;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "studio_booking_id")
    private StudioBooking studioBooking;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "job_assignment_id")
    private JobAssignment jobAssignment;

}
