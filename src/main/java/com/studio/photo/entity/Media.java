package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;


@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"post_id", "path"}))
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Media {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    private String path;
}
