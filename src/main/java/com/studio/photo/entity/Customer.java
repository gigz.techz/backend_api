package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(mappedBy = "customer")
    private User user;

    private String fullName;

    private String address;

    private String city;

    private String profilePicturePath;

    @OneToMany(mappedBy = "customer")
    private List<Feedback> feedbackList;

}
