package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;
import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Studio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(mappedBy = "studio")
    private User user;

    private String studioName;

    private String ownerName;

    private String governmentRegistrationId;

    private String address;

    private String city;

    private String mobileNumber;

    private String profilePicturePath;

    @OneToMany(mappedBy = "studio")
    private List<StudioBooking> studioBookingList;

    @ManyToMany
    @JoinTable(
            name = "studio_chat_participants",
            joinColumns = @JoinColumn(name = "studio_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> chatParticipants;

    @OneToMany(mappedBy = "studio")
    private List<Feedback> feedbackList;



}

