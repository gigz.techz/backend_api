package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Biography {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(mappedBy = "biography")
    private User user;

    private long clubCount;

    private String description;

    private long postCount;

    @OneToMany(mappedBy = "biography", cascade = CascadeType.ALL)
    private List<SocialMediaLink> socialMediaLinks;


}
