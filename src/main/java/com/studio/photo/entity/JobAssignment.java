package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobAssignment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "studio_booking_id")
    private StudioBooking studioBooking;

    @ManyToOne
    @JoinColumn(name = "freelancer_id")
    private Freelancer freelancer;

    @Temporal(TemporalType.TIMESTAMP)
    private Date assignmentDate;

    private double assignmentAmount;

    @OneToMany(mappedBy = "jobAssignment", orphanRemoval = true)
    private Set<StudioServices> studioServices = new LinkedHashSet<>();
}
