package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Mention {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User mentionedUser;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    public Mention(User mentionedUser, Post post) {
        this.mentionedUser = mentionedUser;
        this.post = post;
    }

}
