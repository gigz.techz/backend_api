package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Freelancer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(mappedBy = "freelancer")
    private User user;

    private String fullName;

    private String address;

    private String city;

    private String mobileNumber;

    private String profilePicturePath;

}

