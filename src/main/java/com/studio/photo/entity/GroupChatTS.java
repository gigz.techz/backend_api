package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GroupChatTS {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @ManyToOne
    @JoinColumn(name = "group_chat_id_id")
    private GroupChat groupChatId;

    @ManyToOne
    @JoinColumn(name = "user_id_id")
    private User userId;

    private Date date;
}
