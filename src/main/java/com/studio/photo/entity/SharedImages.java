package com.studio.photo.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SharedImages {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @ManyToOne
    @JoinColumn(name = "shared_by_user_id")
    private User sharedByUser;

    @ManyToOne
    @JoinColumn(name = "shared_with_user_id")
    private User sharedWithUser;
}
