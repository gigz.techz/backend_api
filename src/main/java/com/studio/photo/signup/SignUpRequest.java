package com.studio.photo.signup;

import com.studio.photo.customEnum.UserRole;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter
@Setter
public class SignUpRequest {
    private String  profileImage;
    private String fullName;
    private String address;
    private String city;
    private String username;
    private String password;
    private String mobileNumber;
    private UserRole role;
    private String studioName;
    private String ownerName;
    private String governmentRegistrationId;

    private List<String> serviceList;
}
