package com.studio.photo.signup;

import com.studio.photo.repeat.Response;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.net.UnknownHostException;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/api/public/")
@RequiredArgsConstructor
public class SignUpController {
    private final SignUpService signUpService;

    @PostMapping("signup")
    public SignUpResponse signUp(
            @RequestBody @Valid SignUpRequest signUpRequest,
            @NonNull HttpServletRequest httpServletRequest,
            @NonNull HttpServletResponse response) throws UnknownHostException {
        String baseUrl = getBaseUrl(httpServletRequest);
        return signUpService.signUp(baseUrl,signUpRequest);
    }


    public String getBaseUrl(HttpServletRequest request) {
        StringBuilder baseUrl = new StringBuilder()
                .append(request.getScheme())
                .append("://")
                .append(request.getServerName());
        int port = request.getServerPort();
        if (port != 80 && port != 443) {
            baseUrl.append(":").append(port);
        }
        return baseUrl.toString();
    }

    @GetMapping("verify")
    public ModelAndView verifyLink(
            @RequestParam("string") String token,
            @NonNull HttpServletResponse response) {
        return signUpService.verify(token);
    }

    @PutMapping("signup/password")
    public SignUpResponse setPasswordByUsernameAndAccountVerified(
            @RequestBody @Valid SignUpRequest signUpRequest,
            @NonNull HttpServletRequest httpServletRequest,
            @NonNull HttpServletResponse response) throws UnknownHostException {
        String baseUrl = getBaseUrl(httpServletRequest);
        return signUpService.setPasswordByUsernameAndAccountVerified(baseUrl,signUpRequest);
    }
}