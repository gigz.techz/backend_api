package com.studio.photo.signup;

import com.studio.photo.customEnum.UserRole;
import com.studio.photo.entity.*;
import com.studio.photo.file.FileHandlingService;
import com.studio.photo.mail.UserMailService;
import com.studio.photo.repeat.Response;
import com.studio.photo.repository.*;
import com.studio.photo.security.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;


@RequiredArgsConstructor
@Service
public class SignUpService {
    private final UserRepository userRepository;
    private final FreelancerRepository freelancerRepository;
    private final CustomerRepository customerRepository;
    private final VerifyLinkRepository verifyLinkRepository;
    private final JwtService jwtService;
    private final UserMailService userMailService;
    private final StudioRepository studioRepository;
    private final FileHandlingService fileHandlingService;
    private final UserServicesRepository userServicesRepository;



    @Transactional
    public SignUpResponse signUp(String baseUrl, SignUpRequest signUpRequest)  {
        try {
            String message=null;

            if (signUpRequest.getRole() != UserRole.CUSTOMER)
            {
                if (signUpRequest.getServiceList() == null || signUpRequest.getServiceList().isEmpty()) {
                    throw new RuntimeException("Service list is required");
                }
            }

            if (signUpRequest.getRole().equals(UserRole.FREELANCER)) {
              message=  handleFreelancerSignUp(signUpRequest );
            } else if (signUpRequest.getRole().equals(UserRole.STUDIO)) {
               message= handleStudioSignUp(signUpRequest);
            } else if (signUpRequest.getRole().equals(UserRole.CUSTOMER)) {
               message= handleCustomerSignUp(baseUrl,signUpRequest );
            } else {
                throw new IllegalArgumentException("Invalid signup request");
            }
            return SignUpResponse.builder()
                    .message(message)
                    .build();
        } catch (Exception e) {
            throw new RuntimeException("User registration failed: " + e.getMessage());
        }
    }

    private String handleFreelancerSignUp(SignUpRequest freelancerSignUpRequest) {

        Freelancer freelancer = new Freelancer();
        freelancer.setAddress(freelancerSignUpRequest.getAddress());
        freelancer.setCity(freelancerSignUpRequest.getCity());
        freelancer.setFullName(freelancerSignUpRequest.getFullName());
        freelancer.setProfilePicturePath(freelancerSignUpRequest.getProfileImage());
        freelancer.setMobileNumber(freelancerSignUpRequest.getMobileNumber());
        Freelancer savedFreelancer = freelancerRepository.save(freelancer);

        var user = User.builder()
                .freelancer(savedFreelancer)
                .password(new BCryptPasswordEncoder().encode(freelancerSignUpRequest.getPassword()))
                .role(UserRole.FREELANCER)
                .username(freelancerSignUpRequest.getUsername())
                .active(true)
                .build();
        var post= userRepository.save(user);

        handleUserService(post,freelancerSignUpRequest);

        return post.getRole()+" account created successfully";
    }

    private void handleUserService(User post, SignUpRequest freelancerSignUpRequest) {
        List<String> serviceList = freelancerSignUpRequest.getServiceList();
        if (serviceList != null && !serviceList.isEmpty()) {
            for (String serviceName : serviceList) {
                UserServices userServices = UserServices.builder()
                        .user(post)
                        .serviceName(serviceName)
                        .build();
                userServicesRepository.save(userServices);
            }
        }
    }

    private String handleStudioSignUp(SignUpRequest studioSignUpRequest ) {
        var studio= Studio.builder()
                .studioName(studioSignUpRequest.getStudioName())
                .profilePicturePath(studioSignUpRequest.getProfileImage())
                .address(studioSignUpRequest.getAddress())
                .city(studioSignUpRequest.getCity())
                .governmentRegistrationId(studioSignUpRequest.getGovernmentRegistrationId())
                .mobileNumber(studioSignUpRequest.getMobileNumber())
                .ownerName(studioSignUpRequest.getOwnerName())
                .build();
     var post =   studioRepository.save(studio);
        var user = User.builder()
                .studio(post)
                .username(studioSignUpRequest.getUsername())
                .role(UserRole.STUDIO)
                .password(new BCryptPasswordEncoder().encode(studioSignUpRequest.getPassword()))
                .active(true)
                .build();
       var post1= userRepository.save(user);
        handleUserService(post1,studioSignUpRequest);

        return user.getRole()+" account created successfully";


    }
    private String handleCustomerSignUp(String baseUrl, SignUpRequest customerSignUpRequest ) {
        var customer = Customer.builder()
                .profilePicturePath(customerSignUpRequest.getProfileImage())
                .address(customerSignUpRequest.getAddress())
                .city(customerSignUpRequest.getCity())
                .fullName(customerSignUpRequest.getFullName())
                .build();
       var post= customerRepository.save(customer);
       String password = new User().getRandomPassword();
        var user = User.builder()
                .customer(post)
                .role(UserRole.CUSTOMER)
                .username(customerSignUpRequest.getUsername())
                .password(new BCryptPasswordEncoder().encode(password))
                .active(false)
                .build();
         userRepository.save(user);

         boolean sendMail= userMailService.sendPassword(user.getUsername(),password);
         if(sendMail)
         {
             return user.getRole()+" account created successfully but not verified";

         }
         else {
             throw new RuntimeException("something went wrong ,while sending mail, however account is created");
         }



    }

    public ModelAndView verify(String token) {
        try
        {
           String username = jwtService.extractUsername(token);
           var user = userRepository.findByUsername(username).orElseThrow();
           boolean expired= jwtService.isTokenValid(token,user);
           boolean verifyLink = verifyLinkRepository.existsByUserAndVerifyToken(user,token);
           if(expired && verifyLink)
           {
               user.setAccountVerified(true);
               userRepository.save(user);
               verifyLinkRepository.deleteByUser(user);
               return new ModelAndView("verification-success");
           }
           else
           {
               return new ModelAndView("verification-failed");
           }

        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    public SignUpResponse setPasswordByUsernameAndAccountVerified(String baseUrl, SignUpRequest signUpRequest) {
        try
        {
            var userExistAndIsAccountVerified =  userRepository.existsByUsernameAndRoleAndAccountVerified(signUpRequest.getUsername(),UserRole.CUSTOMER,true);
            if(userExistAndIsAccountVerified)
            {
                var user = userRepository.
                        updatePasswordAndActiveByUsername(new BCryptPasswordEncoder().encode(signUpRequest.getPassword()),true,signUpRequest.getUsername());
                return SignUpResponse.builder()
                        .message(UserRole.CUSTOMER+" Account created successfully")
                        .build();
            }
            else
            {
                throw new RuntimeException("user does not exist:  ");
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

}

