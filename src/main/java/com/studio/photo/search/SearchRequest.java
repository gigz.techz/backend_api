package com.studio.photo.search;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class SearchRequest {
    private String name;
    private String type;
    private String city;
    private Date availability;
    private List<String> serviceName;
}
