package com.studio.photo.search;


import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/protected/search")
@RequiredArgsConstructor
public class SearchController {

    private final SearchServices searchServices;
    @PostMapping("/profile")
    public ResponseEntity<Page<SearchResponseDTO>> searchStudiosAndFreelancers(
            @RequestBody SearchRequest searchRequest,
            @RequestParam("query") String query,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        Page<SearchResponseDTO> result = searchServices.searchByStudio(searchRequest, query, PageRequest.of(page,size));
        return ResponseEntity.ok(result);
    }
}
