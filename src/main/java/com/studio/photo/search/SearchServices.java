package com.studio.photo.search;

import com.studio.photo.customEnum.UserRole;
import com.studio.photo.dto.UserDTO;
import com.studio.photo.repository.CustomerRepository;
import com.studio.photo.repository.FreelancerRepository;
import com.studio.photo.repository.StudioRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Service
public class SearchServices {

    private final CustomerRepository customerRepository;
    private final FreelancerRepository freelancerRepository;
    private final StudioRepository studioRepository;
    private final UserDTO userDTO;

    public Page<SearchResponseDTO> searchByStudio(SearchRequest searchRequest, String query, Pageable pageable) {
        try {
            UserRole userRole = userDTO.getUserRole();
            if(userRole.equals(UserRole.STUDIO))
            {
                if (StringUtils.hasText(query)) {
                    List<SearchResponseDTO> customerResults = customerRepository.findByFullNameAllIgnoreCase(query,pageable)
                            .stream()
                            .map(SearchResponseDTO::fromCustomerEntity)
                            .toList();

                    List<SearchResponseDTO> freelancerResults = freelancerRepository.findByFullNameAllIgnoreCase(query,pageable)
                            .stream()
                            .map(SearchResponseDTO::fromFreelancerEntity)
                            .toList();


                    List<SearchResponseDTO> studioResults = studioRepository.findByStudioNameAllIgnoreCase(query, pageable)
                            .stream()
                            .map(SearchResponseDTO::fromStudioEntity)
                            .toList();

                    List<SearchResponseDTO> combinedResults = Stream.concat(customerResults.stream(),
                                    Stream.concat(freelancerResults.stream(), studioResults.stream()))
                            .collect(Collectors.toList());

                    if (StringUtils.hasText(searchRequest.getType())) {
                        combinedResults = combinedResults.stream()
                                .filter(dto -> dto.getType().equalsIgnoreCase(searchRequest.getType()))
                                .collect(Collectors.toList());
                    }

                    if (StringUtils.hasText(searchRequest.getCity())) {
                        combinedResults = combinedResults.stream()
                                .filter(dto -> dto.getCity().equalsIgnoreCase(searchRequest.getCity()))
                                .collect(Collectors.toList());
                    }

                    if (searchRequest.getServiceName() != null && !searchRequest.getServiceName().isEmpty()) {
                        Set<String> requestedServiceNames = new HashSet<>(searchRequest.getServiceName());
                        combinedResults = combinedResults.stream()
                                .filter(dto -> {
                                    List<String> studioServiceNames = dto.getServiceNames();
                                    return studioServiceNames != null && new HashSet<>(studioServiceNames).containsAll(requestedServiceNames);
                                })
                                .collect(Collectors.toList());
                    }
                    int start = (int) pageable.getOffset();
                    int end = Math.min((start + pageable.getPageSize()), combinedResults.size());

                    return new PageImpl<>(combinedResults.subList(start, end), pageable, combinedResults.size());
                } else {
                    return new PageImpl<>(Collections.emptyList(), pageable, 0);
                }
            } else if (userRole.equals(UserRole.FREELANCER)) {
                if (StringUtils.hasText(query)) {

                    List<SearchResponseDTO> studioResults = studioRepository.findByStudioNameAllIgnoreCase(query, pageable)
                            .stream()
                            .map(SearchResponseDTO::fromStudioEntity)
                            .toList();

                    if (StringUtils.hasText(searchRequest.getType())) {
                        studioResults = studioResults.stream()
                                .filter(dto -> dto.getType().equalsIgnoreCase(searchRequest.getType()))
                                .collect(Collectors.toList());
                    }

                    if (StringUtils.hasText(searchRequest.getCity())) {
                        studioResults = studioResults.stream()
                                .filter(dto -> dto.getCity().equalsIgnoreCase(searchRequest.getCity()))
                                .collect(Collectors.toList());
                    }

                    if (searchRequest.getServiceName() != null && !searchRequest.getServiceName().isEmpty()) {
                        Set<String> requestedServiceNames = new HashSet<>(searchRequest.getServiceName());
                        studioResults = studioResults.stream()
                                .filter(dto -> {
                                    List<String> studioServiceNames = dto.getServiceNames();
                                    return studioServiceNames != null && new HashSet<>(studioServiceNames).containsAll(requestedServiceNames);
                                })
                                .collect(Collectors.toList());
                    }
                    int start = (int) pageable.getOffset();
                    int end = Math.min((start + pageable.getPageSize()), studioResults.size());

                    return new PageImpl<>(studioResults.subList(start, end), pageable, studioResults.size());
                } else {
                    return new PageImpl<>(Collections.emptyList(), pageable, 0);
                }
            }
            else {
                if (StringUtils.hasText(query)) {
                    List<SearchResponseDTO> studioResults = studioRepository.findByStudioNameAllIgnoreCase(query, pageable)
                            .stream()
                            .map(SearchResponseDTO::fromStudioEntity)
                            .toList();


                    if (StringUtils.hasText(searchRequest.getType())) {
                        studioResults = studioResults.stream()
                                .filter(dto -> dto.getType().equalsIgnoreCase(searchRequest.getType()))
                                .collect(Collectors.toList());
                    }

                    if (StringUtils.hasText(searchRequest.getCity())) {
                        studioResults = studioResults.stream()
                                .filter(dto -> dto.getCity().equalsIgnoreCase(searchRequest.getCity()))
                                .collect(Collectors.toList());
                    }

                    if (searchRequest.getServiceName() != null && !searchRequest.getServiceName().isEmpty()) {
                        Set<String> requestedServiceNames = new HashSet<>(searchRequest.getServiceName());
                        studioResults = studioResults.stream()
                                .filter(dto -> {
                                    List<String> studioServiceNames = dto.getServiceNames();
                                    return studioServiceNames != null && new HashSet<>(studioServiceNames).containsAll(requestedServiceNames);
                                })
                                .collect(Collectors.toList());
                    }
                    int start = (int) pageable.getOffset();
                    int end = Math.min((start + pageable.getPageSize()), studioResults.size());

                    return new PageImpl<>(studioResults.subList(start, end), pageable, studioResults.size());
                } else {
                    return new PageImpl<>(Collections.emptyList(), pageable, 0);
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
}
