package com.studio.photo.search;

import com.studio.photo.customEnum.UserRole;
import com.studio.photo.entity.Customer;
import com.studio.photo.entity.Freelancer;
import com.studio.photo.entity.Studio;
import lombok.Data;

import java.util.List;

@Data
public class SearchResponseDTO {
    private Integer id;
    private String name;
    private String address;
    private String city;
    private String profilePicturePath;
    private String type;
    private List<String> serviceNames;

    public static SearchResponseDTO fromCustomerEntity(Customer customer) {
        SearchResponseDTO dto = new SearchResponseDTO();
        dto.setId(customer.getUser().getId());
        dto.setName(customer.getFullName());
        dto.setAddress(customer.getAddress());
        dto.setCity(customer.getCity());
        dto.setProfilePicturePath(customer.getProfilePicturePath());
        dto.setType(String.valueOf(UserRole.CUSTOMER));
        return dto;
    }

    public static SearchResponseDTO fromFreelancerEntity(Freelancer freelancer) {
        SearchResponseDTO dto = new SearchResponseDTO();
        dto.setId(freelancer.getUser().getId());
        dto.setName(freelancer.getFullName());
        dto.setAddress(freelancer.getAddress());
        dto.setCity(freelancer.getCity());
        dto.setType(String.valueOf(UserRole.FREELANCER));
        dto.setProfilePicturePath(freelancer.getProfilePicturePath());
        return dto;
    }

    public static SearchResponseDTO fromStudioEntity(Studio studio) {
        SearchResponseDTO dto = new SearchResponseDTO();
        dto.setId(studio.getUser().getId());
        dto.setName(studio.getStudioName());
        dto.setAddress(studio.getAddress());
        dto.setCity(studio.getCity());
        dto.setType(String.valueOf(UserRole.STUDIO));
        dto.setProfilePicturePath(studio.getProfilePicturePath());
        return dto;
    }
}
