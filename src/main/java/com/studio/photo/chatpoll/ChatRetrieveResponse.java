package com.studio.photo.chatpoll;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatRetrieveResponse {
    private Integer userId;
    private String username;
    private String fullName;
    private String message;
    private LocalDateTime timestamp;
}
