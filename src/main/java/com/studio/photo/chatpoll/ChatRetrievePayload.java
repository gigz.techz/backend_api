package com.studio.photo.chatpoll;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatRetrievePayload {
    private Integer groupId;
    private Integer messageSenderId;
    private LocalDateTime timestamp;

}
