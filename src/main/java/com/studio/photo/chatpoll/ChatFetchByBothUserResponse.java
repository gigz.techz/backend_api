package com.studio.photo.chatpoll;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatFetchByBothUserResponse {
    private Integer chatId;
    private Integer senderId;
    private Integer receiverId;
    private String senderFullName;
    private String receiverFullName;
    private String message;
    private LocalDateTime timestamp;
}
