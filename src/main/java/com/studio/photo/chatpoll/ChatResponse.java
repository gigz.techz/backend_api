package com.studio.photo.chatpoll;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatResponse {

    private String profilePic;

    private Integer userId;

    private String username;

    private String fullName;
}
