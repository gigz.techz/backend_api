package com.studio.photo.chatpoll;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GroupDToResponse {
    private Integer groupId;
    private String groupName;
    private String groupPic;
}
