package com.studio.photo.chatpoll;

import com.studio.photo.chat.GroupRequest;
import com.studio.photo.dto.ChatDTO;
import com.studio.photo.dto.UserDTO;
import com.studio.photo.entity.ChatMessage;
import com.studio.photo.entity.GroupChat;
import com.studio.photo.entity.User;
import com.studio.photo.entity.UsersChatTS;
import com.studio.photo.notification.NotificationService;
import com.studio.photo.repeat.Response;
import com.studio.photo.repository.ChatMessageRepository;
import com.studio.photo.repository.GroupChatRepository;
import com.studio.photo.repository.NotificationRepository;
import com.studio.photo.repository.UserChatTSRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ChatPService {

    private final ChatMessageRepository chatMessageRepository;
    private final GroupChatRepository groupChatRepository;
    private final UserDTO userDTO;
    private final UserChatTSRepository userChatTSRepository;
    private final NotificationService notificationService;

    public Response createGroup(GroupRequest groupRequest) {
        User currentUser = userDTO.getCurrentLoginUser();

        List<User> participants = userDTO.getUsersByIds(groupRequest.getParticipantIds());
        participants.add(currentUser);

        GroupChat groupChat = GroupChat.builder()
                .groupName(groupRequest.getGroupName())
                .groupPic(groupRequest.getGroupImage())
                .groupDescription(groupRequest.getGroupDescription())
                .admin(currentUser)
                .participants(participants)
                .build();

        groupChat = groupChatRepository.save(groupChat);
        notificationService.pushNotify(participants, "You have been added to a new group: " + groupChat.getGroupName());

        return Response.builder()
                .message("group created successfully")
                .build();
    }

    @Transactional
    public Response addParticipantToGroup(Integer groupId, List<Integer> participantIds) {
        Optional<GroupChat> optionalGroupChat = groupChatRepository.findById(groupId);

        if (optionalGroupChat.isPresent()) {
            GroupChat groupChat = optionalGroupChat.get();

            User currentUser = userDTO.getCurrentLoginUser();
            if (currentUser.getId().equals(groupChat.getAdmin().getId())) {
                List<User> participants = userDTO.getUsersByIds(participantIds);
                for (User participant : participants) {
                    groupChat.addParticipant(participant);
                }
                groupChatRepository.save(groupChat);
                notificationService.pushNotify(participants, "You have been added to the group: " + groupChat.getGroupName());

                return Response.builder()
                        .message("success")
                        .build();
            } else {
                throw new RuntimeException("Only the group admin can add participants");
            }
        } else {
            throw new RuntimeException("Group does not exist");
        }
    }

    public Page<GroupDToResponse> getAllGroup(Pageable page) {
        User currentUser = userDTO.getCurrentLoginUser();
        Page<GroupChat> userGroupsPage = groupChatRepository.findByParticipantsContaining(currentUser.getId(), page);

        return userGroupsPage.map(groupChat -> GroupDToResponse.builder()
                .groupId(groupChat.getId())
                .groupName(groupChat.getGroupName())
                .groupPic(groupChat.getGroupPic())
                .build());
    }

    @Transactional
    public Response removeMembersFromGroup(Integer groupId, List<Integer> participantIds) {
        User currentUser = userDTO.getCurrentLoginUser();

        Optional<GroupChat> optionalGroupChat = groupChatRepository.findById(groupId);

        if (optionalGroupChat.isPresent()) {
            GroupChat groupChat = optionalGroupChat.get();

            if (currentUser.getId().equals(groupChat.getAdmin().getId())) {
                List<User> participantsToRemove = userDTO.getUsersByIds(participantIds);

                groupChat.getParticipants().removeAll(participantsToRemove);

                groupChatRepository.save(groupChat);
                notificationService.pushNotify(participantsToRemove, "You have been removed from the group: " + groupChat.getGroupName());
                return Response.builder()
                        .message("Members removed successfully")
                        .build();
            } else {
                throw new RuntimeException("Only the group admin can remove members");
            }
        } else {
            throw new RuntimeException("Group does not exist");
        }
    }

    @Transactional
    public Response chatSendMessageOneToOne(ChatSendRequest chatSendRequest) {
        User currentUser = userDTO.getCurrentLoginUser();
        Integer receiverUserId = chatSendRequest.getReceiverUserId();
        Optional<User> optionalReceiver = Optional.ofNullable(userDTO.getUserById(receiverUserId));

        if (optionalReceiver.isPresent()) {
            User receiver = optionalReceiver.get();
            ChatMessage chatMessage = ChatMessage.builder()
                    .sender(currentUser)
                    .receiver(receiver)
                    .message(chatSendRequest.getMessage())
                    .timestamp(LocalDateTime.now())
                    .build();

          var pos=  chatMessageRepository.save(chatMessage);

            List<User> users = Arrays.asList(currentUser, receiver);
            for (User user : users) {
                UsersChatTS timestampEntry = userChatTSRepository.findByChatMessageAndUser(chatMessage,user)
                        .orElseGet(() -> UsersChatTS.builder()
                                .user(user)
                                .chatMessage(chatMessage)
                                .build());

//                timestampEntry.setDate(new Date());
                userChatTSRepository.save(timestampEntry);
            }

            return Response.builder()
                    .message("Message sent successfully")
                    .build();
        } else {
            throw new RuntimeException("Receiver user not found");
        }
    }

    public Page<ChatResponse> getAllChat(Pageable page) {
        User currentUser = userDTO.getCurrentLoginUser();
        Page<ChatMessage> userChatMessagesPage = chatMessageRepository.findBySenderOrReceiver(currentUser, currentUser, page);

        Set<Integer> uniqueUserIds = new HashSet<>();
        List<ChatResponse> uniqueResponses = new ArrayList<>();

        for (ChatMessage chatMessage : userChatMessagesPage.getContent()) {
            User otherUser = (currentUser.equals(chatMessage.getSender())) ? chatMessage.getReceiver() : chatMessage.getSender();
            if (otherUser != null) {
                String fullName = getFullNameBasedOnUserRole(otherUser);
                String profilePic = getProfilePicBasedOnROle(otherUser);

                if (uniqueUserIds.add(otherUser.getId())) {
                    uniqueResponses.add(ChatResponse.builder()
                            .profilePic(profilePic)
                            .userId(otherUser.getId())
                            .username(otherUser.getUsername())
                            .fullName(fullName)
                            .build());
                }
            }
        }

        return new PageImpl<>(uniqueResponses, userChatMessagesPage.getPageable(), userChatMessagesPage.getTotalElements());
    }

    private String getProfilePicBasedOnROle(User user) {
        if (user.getCustomer() != null) {
            return user.getCustomer().getProfilePicturePath();
        } else if (user.getFreelancer() != null) {
            return user.getFreelancer().getProfilePicturePath();
        } else {
            return user.getStudio().getProfilePicturePath();
        }
    }

    private String getFullNameBasedOnUserRole(User user) {
        if (user.getCustomer() != null) {
            return user.getCustomer().getFullName();
        } else if (user.getFreelancer() != null) {
            return user.getFreelancer().getFullName();
        } else {
            return user.getStudio().getOwnerName();
        }
    }

    public Response sendGroupMessage(GroupChatMessageRequest groupChatMessageRequest) {
        User currentUser = userDTO.getCurrentLoginUser();
        GroupChat groupChat = groupChatRepository.findById(groupChatMessageRequest.getGroupId())
                .orElseThrow(() -> new EntityNotFoundException("Group not found"));

        if (!groupChat.getParticipants().contains(currentUser)) {
            return Response.builder().message("User is not a participant in the group").build();
        }

        ChatMessage chatMessage = ChatMessage.builder()
                .sender(currentUser)
                .groupChat(groupChat)
                .message(groupChatMessageRequest.getMessage())
                .timestamp(LocalDateTime.now())
                .build();

        chatMessageRepository.save(chatMessage);

        return Response.builder().message("Message sent to group successfully").build();
    }

    public List<ChatRetrieveResponse> receiveGroupMessage(ChatRetrievePayload payload, Pageable pageable) {
        User currentUser = userDTO.getCurrentLoginUser();
        GroupChat groupChat = groupChatRepository.findById(payload.getGroupId())
                .orElseThrow(() -> new EntityNotFoundException("Group not found"));

        if (!groupChat.getParticipants().contains(currentUser)) {
            throw new AccessDeniedException("User is not a participant in the group");
        }

        Page<ChatMessage> groupMessages = chatMessageRepository.findByGroupChatOrderByTimestampAsc(
                groupChat,
                pageable
        );

        List<ChatRetrieveResponse> chatRetrieveResponses = new ArrayList<>();

        for (ChatMessage chatMessage : groupMessages) {
            User sender = chatMessage.getSender();
            String fullName = getFullNameBasedOnUserRole(sender);

            ChatRetrieveResponse chatRetrieveResponse = ChatRetrieveResponse.builder()
                    .userId(sender.getId())
                    .username(sender.getUsername())
                    .fullName(fullName)
                    .message(chatMessage.getMessage())
                    .timestamp(chatMessage.getTimestamp())
                    .build();

            chatRetrieveResponses.add(chatRetrieveResponse);
        }

        return chatRetrieveResponses;
    }


    public Page<ChatFetchByBothUserResponse> fetchMessageByEitherSenderOrReceiver(ChatRetrievePayload payload, Pageable pageable) {
        User currentUser = userDTO.getCurrentLoginUser();
        User otherUser = userDTO.getUserById(payload.getMessageSenderId());

        Page<ChatMessage> messagesByEitherUser = chatMessageRepository
                .findBySenderAndReceiverOrSenderAndReceiverOrderByTimestampAsc(
                        currentUser, otherUser, otherUser, currentUser, pageable);

        return messagesByEitherUser.map(chatMessage -> {
            User sender = chatMessage.getSender();
            User receiver = chatMessage.getReceiver();

            String senderFullName = getFullNameBasedOnUserRole(sender);
            String receiverFullName = getFullNameBasedOnUserRole(receiver);

            return ChatFetchByBothUserResponse.builder()
                    .chatId(chatMessage.getId())
                    .senderId(sender.getId())
                    .receiverId(receiver.getId())
                    .senderFullName(senderFullName)
                    .receiverFullName(receiverFullName)
                    .message(chatMessage.getMessage())
                    .timestamp(chatMessage.getTimestamp())
                    .build();
        });
    }

    public Response deleteChatMessageById(Integer chatId) {
        ChatMessage chatMessage = chatMessageRepository.findById(chatId)
                .orElseThrow(() -> new EntityNotFoundException("Chat message not found"));

        User currentUser = userDTO.getCurrentLoginUser();
        if (!chatMessage.getSender().equals(currentUser)) {
            return Response.builder().message("You can only delete messages you have sent.").build();
        }

        chatMessageRepository.deleteById(chatId);

        return Response.builder().message("Chat message deleted successfully.").build();
    }

    @Transactional
    public Response deleteGroup(Integer groupId) {
        User currentUser = userDTO.getCurrentLoginUser();
        GroupChat groupChat = groupChatRepository.findById(groupId)
                .orElseThrow(() -> new EntityNotFoundException("Group not found"));
        if (!currentUser.equals(groupChat.getAdmin())) {
            return Response.builder().message("Only the admin can delete the group.").build();
        }

        groupChatRepository.delete(groupChat);
        return Response.builder().message("Group deleted successfully.").build();
    }
}
