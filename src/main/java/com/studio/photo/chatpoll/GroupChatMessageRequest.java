package com.studio.photo.chatpoll;

import lombok.*;

@Data
public class GroupChatMessageRequest {
    private Integer groupId;
    private String message;
}

