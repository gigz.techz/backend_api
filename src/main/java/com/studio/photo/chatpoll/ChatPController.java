package com.studio.photo.chatpoll;

import com.studio.photo.chat.GroupRequest;
import com.studio.photo.repeat.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/protected/v1/chat")
@RequiredArgsConstructor
public class ChatPController {

    private final ChatPService chatPService;
    @PostMapping("create-group")
    public Response createGroup(
            @RequestBody GroupRequest groupRequest
    ) {
        return chatPService.createGroup(groupRequest);
    }

    @PostMapping("add-member-in-group")
    public Response addParticipantToGroup(
            @RequestBody GroupRequest groupRequest
    ) {
        return chatPService.addParticipantToGroup(groupRequest.getGroupId(), groupRequest.getParticipantIds());
    }

    @GetMapping("get-groups")
    public Page<GroupDToResponse> getAllGroups(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        return chatPService.getAllGroup(PageRequest.of(page, size));
    }

    @DeleteMapping("remove-members-from-group")
    public Response removeMembersFromGroup(
            @RequestBody GroupRequest groupRequest

    ) {
        return chatPService.removeMembersFromGroup(groupRequest.getGroupId(), groupRequest.getParticipantIds());
    }

    @PostMapping("send-message")
    public Response sendMessage(
            @RequestBody ChatSendRequest chatSendRequest

    ) {
        return chatPService.chatSendMessageOneToOne(chatSendRequest);
    }


    @GetMapping("get-all-chat")
    public Page<ChatResponse> getAllChat(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        return chatPService.getAllChat(PageRequest.of(page, size));
    }


    @PostMapping("get-message")
    public Page<ChatFetchByBothUserResponse> fetchMessageByEitherSenderOrReceiver(
            @RequestBody ChatRetrievePayload payload,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size

    ) {
        return chatPService.fetchMessageByEitherSenderOrReceiver(payload,PageRequest.of(page, size));
    }


    @PostMapping("send-group-message")
    public Response sendGroupMessage(
            @RequestBody GroupChatMessageRequest groupChatMessageRequest
    ) {
        return chatPService.sendGroupMessage(groupChatMessageRequest);
    }


    @PostMapping("receive-group-message")
    public List<ChatRetrieveResponse> receiveGroupMessage(
            @RequestBody ChatRetrievePayload payload,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size

    ) {
        return chatPService.receiveGroupMessage(payload,PageRequest.of(page,size));
    }


    @DeleteMapping("delete-chat/{chatId}")
    public Response deleteChatMessageById(@PathVariable Integer chatId) {
        return chatPService.deleteChatMessageById(chatId);
    }

    @DeleteMapping("delete-group/{groupId}")
    public Response deleteGroup(@PathVariable Integer groupId) {
        return chatPService.deleteGroup(groupId);
    }


}
