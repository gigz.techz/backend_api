package com.studio.photo.chatpoll;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChatSendRequest {
    private String message;
    private Integer receiverUserId;

}
