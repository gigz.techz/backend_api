package com.studio.photo.security;

import com.studio.photo.chat.ChatRequest;
import com.studio.photo.chat.MessageType;
import com.studio.photo.dto.UserDTO;
import com.studio.photo.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
@Slf4j
@RequiredArgsConstructor
public class WebSocketEventListener {

    private final SimpMessageSendingOperations messagingTemplate;
    private final UserDTO userDTO;

    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        String username = (String) headerAccessor.getSessionAttributes().get("username");
        if (username != null) {
            User currentUser = userDTO.getCurrentLoginUser();

            ChatRequest chatRequest = ChatRequest.builder()
                    .type(MessageType.LEAVE)
                    .content("Your content")
                    .receiverId(123)
                    .build();

            messagingTemplate.convertAndSend("/topic/public", chatRequest);

            messagingTemplate.convertAndSendToUser(
                    String.valueOf(currentUser.getId()),
                    "/private",
                    "You have been disconnected."
            );
        }
    }
}

