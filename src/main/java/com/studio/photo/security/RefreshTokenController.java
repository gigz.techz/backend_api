package com.studio.photo.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.studio.photo.exception.ErrorPOJA;
import com.studio.photo.repository.UserRepository;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class RefreshTokenController {

    private final UserRepository userRepository;

    private final JwtService jwtService;

    @PostMapping("/refreshToken")
    public HashMap<String,String> getRefreshToken(@RequestBody  RefreshTokenRequest request  ,
            @NonNull HttpServletResponse response) throws IOException {
        try {
           Integer id = Integer.valueOf(jwtService.extractUsername(request.getRefresh_token()));
            boolean user= userRepository.existsById(id);
            HashMap<String,String> hashMap = new HashMap<String,String>();
            boolean checkRefresh= jwtService.isRefreshTokenValid(request.getRefresh_token());

            if(user && checkRefresh){
                var userRefreshToken= userRepository.findById(id).orElseThrow();
                hashMap.put("bearer_token",jwtService.generateToken((UserDetails) userRefreshToken));
                 hashMap.put("refresh_token",jwtService.generateToken(id));
                return hashMap;
            }
            else{
               throw new RuntimeException("invalid ");
            }
        }
        catch (Exception e)
        {
            ErrorPOJA errorPOJO= new ErrorPOJA();
            Date date = new Date();
            errorPOJO.setError_description("login controller failed "+e.getLocalizedMessage());
            errorPOJO.setUser_description("login declined");
            response.setStatus(FORBIDDEN.value());
            errorPOJO.setCode(String.valueOf(response.getStatus()));
            response.setContentType(APPLICATION_JSON_VALUE);
            new ObjectMapper().writeValue(response.getOutputStream(),errorPOJO);
            return null;
        }
    }

}
