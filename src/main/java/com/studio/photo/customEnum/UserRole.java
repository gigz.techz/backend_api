package com.studio.photo.customEnum;

public enum UserRole {
    CUSTOMER,
    STUDIO,
    FREELANCER;
}
