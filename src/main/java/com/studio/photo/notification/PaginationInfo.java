package com.studio.photo.notification;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class PaginationInfo {
    private final int currentPage;
    private final int pageSize;
    private final long totalItems;
    private final int totalPages;
    private final boolean isFirstPage;
    private final boolean isLastPage;
}
