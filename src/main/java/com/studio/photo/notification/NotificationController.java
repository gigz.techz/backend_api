package com.studio.photo.notification;

import com.studio.photo.repeat.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/api/protected/notification")
@RequiredArgsConstructor
public class NotificationController {

    private final NotificationService notificationService;

    @GetMapping("push-notify")
    public NotificationResponse pushNotify(@RequestParam(defaultValue = "0") int page,
                                           @RequestParam(defaultValue = "10") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return notificationService.getPushNotify(pageable);
    }

    @PutMapping("push-notify")
    public Response updateNotification(@RequestBody NotificationUpdateRequest notificationUpdateRequest) {

        return notificationService.updateNotification(notificationUpdateRequest);
    }

}
