package com.studio.photo.notification;

import com.studio.photo.dto.UserDTO;
import com.studio.photo.entity.Notification;
import com.studio.photo.entity.User;
import com.studio.photo.repeat.Response;
import com.studio.photo.repository.NotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NotificationService {
    private final NotificationRepository notificationRepository;
    private final UserDTO userDTO;
    public void pushNotify(List<User> participants, String message) {
        for (User participant : participants) {
            Notification notification = Notification.builder()
                    .user(participant)
                    .message(message)
                    .timestamp(LocalDateTime.now())
                    .read(false)
                    .build();

            notificationRepository.save(notification);
        }

    }

    public void pushNotify(NotificationRequest notificationRequest) {
        try {
            List<Integer> userIds = notificationRequest.getUserIds();
            String message = notificationRequest.getMessage();

            for (Integer userId : userIds) {
                User participant = userDTO.getUserById(userId);
                if (participant != null) {
                    Notification notification = Notification.builder()
                            .user(participant)
                            .message(message)
                            .timestamp(LocalDateTime.now())
                            .read(false)
                            .build();

                    notificationRepository.save(notification);
                } else {

                }
            }

        } catch (Exception e) {
            throw new RuntimeException(" error in notification "+ Arrays.toString(e.getStackTrace()));
        }
    }

    public NotificationResponse getPushNotify(Pageable pageable)  {
        try {
            User currentUser = userDTO.getCurrentLoginUser();

            Page<Notification> notificationPage = notificationRepository.findByUserAndRead(currentUser, false, pageable);
            List<NotificationResponse.NotificationData> notificationDataList = notificationPage.getContent().stream()
                    .map(NotificationResponse.NotificationData::fromEntity)
                    .collect(Collectors.toList());

            return NotificationResponse.builder()
                    .success(true)
                    .notification(notificationDataList)
                    .paginationInfo(createPaginationInfo(notificationPage))
                    .build();
        } catch (Exception e) {
            return NotificationResponse.builder()
                    .success(false)
                    .notification(null)
                    .paginationInfo(null)
                    .build();
        }
    }


    private PaginationInfo createPaginationInfo(Page<?> page) {
        return new PaginationInfo(
                page.getNumber(),
                page.getSize(),
                page.getTotalElements(),
                page.getTotalPages(),
                page.isFirst(),
                page.isLast());
    }


    public Response updateNotification(NotificationUpdateRequest notificationUpdateRequest) {
        try {
            List<Integer> notificationIds = notificationUpdateRequest.getNotificationIds();
            if (notificationIds.isEmpty()) {
                throw new RuntimeException(" error occurred : notification is empty ");

            }

            List<Notification> notificationsToUpdate = notificationRepository.findByNotificationIdsAndRead(notificationIds, false);
            for (Notification notification : notificationsToUpdate) {
                notification.setRead(true);
            }

            notificationRepository.saveAll(notificationsToUpdate);

            return Response.builder()
                    .message("success")
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(" error occurred "+e.getLocalizedMessage());
        }
    }

    public boolean doesNotificationExist(Integer userId, String message) {
       return notificationRepository.existsByUser_IdAndMessageAndReadAllIgnoreCase(userId,message,false);

    }
}
