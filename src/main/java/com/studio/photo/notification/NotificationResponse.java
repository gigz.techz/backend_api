package com.studio.photo.notification;

import com.studio.photo.entity.Notification;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationResponse {
    private boolean success;
    private List<NotificationData> notification;
    private PaginationInfo paginationInfo;


    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class NotificationData {
        private Integer id;
        private String message;
        private LocalDateTime timestamp;

        public static NotificationData fromEntity(Notification notification) {
            return NotificationData.builder()
                    .id(notification.getId())
                    .message(notification.getMessage())
                    .timestamp(notification.getTimestamp())
                    .build();
        }
    }
}
