package com.studio.photo.notification;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationUpdateRequest {
    private List<Integer> notificationIds;
}
